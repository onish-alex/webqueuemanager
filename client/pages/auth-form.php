<div class="auth-area">
    <div class="reg-area">
        <p>Логин:</p>
        <input type="text" name="reg-login" id="reg-login">
    </div>
    <div class="login-email">
        <p id="login-email-label">E-mail:</p>
        <input type="email" name="login-email" id="login-email">   
    </div>
    <div class="password">
        <p>Пароль:</p>
        <input type="password" name="password" id="password">    
    </div>
    <div class="submit-reg-auth">
        <button id="sign-in-button">Войти</button>
    </div>
    <div class="switch-mode">
        <button id="switch-mode-button">Создать аккаунт</button>
    </div>
    <div class="error-message">
    </div>
</div>