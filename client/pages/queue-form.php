<div class="queue-form">
            <div class="queue-main-info">
                <div class="queue-info-area">
                    <p class="queue-name">ITeh</p>
                    <div class="labels">
                        <p>Время начала: </p>
                    </div>
                    <div class="data">
                        <p class="queue-event-date">31.05.2020</p>
                    </div>
                    <div class="labels">
                        <p>Место проведения события:</p>
                    </div>
                    <div class="data">
                        <p class="queue-place">dfdg</p>
                    </div>
                    <div class="labels">
                        <p>Описание:</p>
                    </div>
                    <div class="queue-description data">
                        <p>dsf</p>
                    </div>
                    <div class="queue-join-area">
                        <button class = "button-pressed" id="button-join-queue">Присоединится к очереди</button>
                    </div>
                    <div class="history-button-area">
                        <button id="button-show-history" class="button-pressed">Показать/скрыть историю</button>
                    </div>
                    <div class="update-button-area">
                        <button id="button-update-queue" class="button-pressed">Изменить данные очереди</button>
                    </div>
                    <div class="finish-button-area">
                        <button id="button-finish-queue" class="button-pressed">Закончить приём</button>
                    </div>
                </div>
                <div class="queue-member-item queue-user-item"  >
                    <p class="queue-member-number"></p>
                    <p class="queue-member-name"></p>
                    <button class="queue-member-function">Отойти на время</button>
                </div>
                <div class="queue-members-list">
                    <!--<div class="queue-member-item">
                        <div class="queue-member-number">1453.</div>
                        <div class="queue-member-name">Вася</div>
                        <button class="queue-member-function">Предложить поменятся</button>
                    </div>-->
                </div>  
            </div>
            <div class="chat-area">
                <div class="messages-area">
                    <!--
                    <div class="chat-message other-chat-message">
                        <p class="sender-name">test1</p>
                        <p class="message-text">message1</p>
                    </div>
                    <div class="chat-message my-chat-message">
                        <p class="sender-name">test2</p>
                        <p class="message-text">message2</p>
                    </div>
                    -->
                </div>
                <div class="input-message-area">
                    <input type="text" id="message-input">
                    <button id="button-send-message"><p>Send</p></button>
                </div> 
            </div>
        </div>