<div class="swap-request-form">
    <p class="request-header">Вам пришло предложение поменятся!</p>
    <p class="request-message"></p>
    <div class="request-button-panel">
        <button id="button-accept-request" class="accept-request button-pressed">Принять</button>
        <button id="button-decline-request" class="decline-request button-pressed">Отклонить</button>
        <button id="button-skip-message" class="button-pressed">Понятно</button>
    </div>
</div>