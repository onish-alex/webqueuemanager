<div class="queue-create-area">
    <button id="close-queue-create-area">X</button>
    <div class="new-queue-name">
        <p>Название события:</p>
        <input type="text" id="queue-name-input">
    </div>
    <div class="new-queue-place">
        <p>Место проведения события:</p>
        <input type="text" id="queue-place-input">
    </div>
    <div class="new-queue-date">
        <p>Дата проведения события:</p>
        <input type="datetime-local" id="queue-date-input">
    </div>
    <div class="new-queue-description">
        <p>Описание:</p>
        <textarea id="queue-description-input" maxlength="280" cols="40" rows="7"></textarea>
    </div>
    <div class="submit-queue">
        <button id="button-submit-queue">Создать</button>
    </div>
    <div class="error-message">

    </div>
</div>