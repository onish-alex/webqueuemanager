let indexToChangeState;

function HandleResponse(data) {
    switch (data.type) {

//#region auth 
        case "auth":
            
            if (data.result == 'successful') {
                ToMainForm(data.user_login);

                userLogin = data.user_login;
                userId = data.user_id;

                //отправка запроса на подгрузку первого пакета
                let messageData = {
                    type: "get global queues",
                    sort_field: 'queue_name',
                    last_value: null,
                    last_id: null
                };
        
                socket.send(JSON.stringify(messageData));

                $('#button-load-queues').css('display', 'block');

            }
            else if (data.result == 'invalid login') {
                $(".auth-area .error-message").html("<p>" + authInvalidLogin + "</p>");
            }
            else if (data.result == 'invalid password') {
                $(".auth-area .error-message").html("<p>" + authInvalidPassword + "</p>");
            }
            else if (data.result == 'user auth exist') {
                $(".auth-area .error-message").html("<p>" + authUserExist + "</p>");
            }

            break;
//#endregion

//#region reg
        case "reg":

            if (data.result == 'successful') {
                $(".auth-area .error-message").html("<p style='color: green;'>" + regSuccess + "</p>");
                SwitchRegAuth(authForm);
            }
            else if (data.result == 'invalid login') {
                $(".auth-area .error-message").html("<p>" + regInvalidLogin + "</p>");
            }
            else if (data.result == 'invalid email') {
                $(".auth-area .error-message").html("<p>" + regInvalidEmail + "</p>");
            }
            else if (data.result == 'invalid password') {
                $(".auth-area .error-message").html("<p>" + regInvalidPassword + "</p>");
            }
            else if (data.result == 'exist login') {
                $(".auth-area .error-message").html("<p>" + regExistLogin + "</p>");
            }
            else if (data.result == 'exist email') {
                $(".auth-area .error-message").html("<p>" + regExistEmail + "</p>");
            }

            break;
//#endregion

//#region queue create
        case "queue create":

            if (data.result == 'successful') {
                CreateQueueMode(false);
                userCreatedQueues.push(data.queue);
            }
            else if (data.result == 'invalid lesson_date') {
                $(".queue-create-area .error-message").html("<p>" + queueInvalidDate + "</p>");
            }
            else if (data.result == 'invalid lesson_place') {
                $(".queue-create-area .error-message").html("<p>" + queueInvalidPlace + "</p>");
            }
            else if (data.result == 'invalid queue_name') {
                $(".queue-create-area .error-message").html("<p>" + queueInvalidName + "</p>");
            }
            else if (data.result == 'invalid queue_description') {
                $(".queue-create-area .error-message").html("<p>" + queueInvalidDescription + "</p>");
            }

            break;

//#endregion

//#region get global queues
        case "get global queues":
            FillQueuesList(data.global_queues)
            break;
//#endregion
        
//#region get user queues
        case "get user queues":
            $('.queue-item').off('click', QueueItemClickHandler);
        
            userCreatedQueues = [];
            userJoinedQueues = [];

            str = "";
            for (let item of data.user_queues) {
                
                if (item.user_login == userLogin) {
                    userCreatedQueues.push(item);
                    str += GenerateQueueItemHTML(item);
                    continue;
                }
                
                userJoinedQueues.push(item);
            }
            $(".queues-area").html(str);

            $('.queue-item').on('click', QueueItemClickHandler);

            break;
//#endregion

//#region get queue info
        case "get queue info":
            isCreatorSelectedQueue = (selectedQueueAuthor == userLogin);

                    

            ToQueueForm(data.queue);

            $(".queue-info-area").find('.queue-name').html(data.queue.queue_name);
            $(".queue-info-area").find('.queue-author').html(selectedQueueAuthor);
            $(".queue-info-area").find('.queue-event-date').html(data.queue.lesson_date);
            $(".queue-info-area").find('.queue-place').html(data.queue.lesson_place);
            $(".queue-info-area").find('.queue-description').html(data.queue.queue_description);

            break;
//#endregion

//#region get members
        case "get members":

            userJoinedQueuesMembersCache.set(selectedQueueId, data.queue_members);
            
            $('.queue-user-item .queue-member-function').off('click', OnFreezeClick);
            //$('.queue-members-list .queue-member-function').off('click', OnMemberFunctionClick);
            FillMembersList(data.queue_members);
            $('.queue-user-item .queue-member-function').on('click', OnFreezeClick);
            //$('.queue-members-list .queue-member-function').on('click', OnMemberFunctionClick);
            
            members = userJoinedQueuesMembersCache.get(selectedQueueId);
            currentMember = members.find(function(element, index, array) {
                return (element.user_login == userLogin);
            });

            if (isCreatorSelectedQueue || currentMember == undefined) {
                $(".chat-area").css("display", "none");
                $(".queue-main-info").css("width", "100%");
            } else {
                $(".chat-area").css("display", "flex");
                $(".queue-main-info").css("width", "70%");    
            }

            break;
//#endregion
        
//#region queue join
        case "queue join": 

            if (data.result == 'successful') {
                
               members = userJoinedQueuesMembersCache.get(selectedQueueId);

               //ищем, есть ли другие активные мемберы
                var anotherActiveIndex = -1;
                anotherActiveIndex = members.findIndex(function(element, index, array) {
                    return element.state == '1';
                });
            
                console.log(anotherActiveIndex);

                if(anotherActiveIndex == -1) {
                    members.unshift(data.queue_member);
                } 
                else {
                    members.push(data.queue_member);
                }

                DisplayMembersList(members);

                $(".queue-user-item").css("display", "flex");
                $('.queue-user-item').removeClass("unactive-user");
                $('.queue-user-item .queue-member-number').html(data.queue_member.position);
                $('.queue-user-item .queue-member-name').html(data.queue_member.user_login);
                $('.queue-user-item .queue-member-function').off('click', OnFreezeClick);
                $('.queue-user-item .queue-member-function').html("Отойти на время");
                $('.queue-user-item .queue-member-function').on('click', OnFreezeClick);

                $('.queue-members-list .queue-member-function').css('display', 'block');

                $(".chat-area").css("display", "flex");
                $(".queue-main-info").css("width", "70%");    
                $('.messages-area').html("");

                if (data.queue_member.position == "1") {
                    $('#button-join-queue').css('display', 'none');
                    $('.queue-user-item .queue-member-function').css('display', 'none');
                }

                $('#button-join-queue').html(leaveQueueLabel);

                userJoinedQueuesMembersCache.set(selectedQueueId, members);

                firstMember = members.find(function(element, index, array) {
                    return (element.position == '1');
                });
            
                if (firstMember != undefined) {
                    firstMemberButton = $('#member-id-' + firstMember.member_id).find(".queue-member-function");
                    if (firstMember.user_login != userLogin) {
                        firstMemberButton.css('display', 'none');
                    }
                    else {
                        firstMemberButton.css('display', 'block');
                    }
                }

            }
            
            break;
//#endregion

//#region new queue member (рассылка)
        case "new queue member":

            members = userJoinedQueuesMembersCache.get(selectedQueueId);

            if (members == undefined) {
                return;
            }

            //ищем, есть ли другие активные мемберы
            var anotherActiveIndex = -1;
            anotherActiveIndex = members.findIndex(function(element, index, array) {
                return element.state == '1';
            });
        
            console.log(anotherActiveIndex);

            if(anotherActiveIndex == -1) {
                members.unshift(data.queue_member);
            } 
            else {
                members.push(data.queue_member);
            }

            if (selectedQueueId == data.queue_member.queue_id) {
            
                FillMembersList(members);
                if (!isCreatorSelectedQueue) {
                    if (!isJoinSelectedQueue) {
                        $('.queue-members-list .queue-member-function').css('display', 'none');
                    }
                    else if (data.queue_member.position != '1' && FindCurrentUserByLogin(members).position != '1') {
                        $('#member-id-' + data.queue_member.member_id).find(".queue-member-function").css('display', 'block');
                    }
                }
            
            }

            userJoinedQueuesMembersCache.set(selectedQueueId, members);

            break;

//#endregion

//#region queue leave
        case "queue leave": 
            
            selectedQueueMembers = userJoinedQueuesMembersCache.get(selectedQueueId);
            var memberIndex;
            for (i = 0; i < selectedQueueMembers.length; i++) {
                if (selectedQueueMembers[i].member_id == data.member_id) {
                    memberIndex = i;
                    selectedQueueMembers.splice(i, 1);
                    break;
                }
            }

            for (i = memberIndex; i < selectedQueueMembers.length; i++) {
                selectedQueueMembers[i].position--;
            }

            DisplayMembersList(selectedQueueMembers);

            $(".queue-user-item").css("display", "none");

            if (data.queue_state == '1') {
                $('#button-join-queue').html(joinQueueLabel);
            } 
            else {
                $("#button-join-queue").css('display', 'none');
            }

            $('.messages-area').html("");
            $(".chat-area").css("display", "none");
            $(".queue-main-info").css("width", "100%");

            $('.queue-members-list .queue-member-function').css('display', 'none');

            break;
//#endregion

//#region queue member leave (рассылка)
        case "queue member leave": 

            if (data.second_id == userId) {
                var notification = new Notification(notifySecondMessage, 
                    {body: userLogin + notifySecondMessageBody + '"' + data.queue_name + '"'});
            }

            members = userJoinedQueuesMembersCache.get(selectedQueueId);
            
            if (members == undefined) {
                return;
            }
            
            memberToRemoveIndex = members.findIndex(function(element, index, array) {
                if (element.member_id == data.member_id) {
                    return index;
                }
            });

            members.splice(memberToRemoveIndex, 1);

            for (i = memberToRemoveIndex; i < members.length; i++) {
                members[i].position--;
            }

            userJoinedQueuesMembersCache.set(selectedQueueId, members);

            if (data.queue_id == selectedQueueId) {
                selectedQueueMembers = userJoinedQueuesMembersCache.get(selectedQueueId);
                FillMembersList(selectedQueueMembers);
                let currentMemberPosition = $('.queue-user-item .queue-member-number').text();
                if (currentMemberPosition > memberIndex + 1) {
                    $('.queue-user-item .queue-member-number').html(currentMemberPosition - 1);
                }
            }
            break;
//#endregion
        
//#region get searched queues
        case "get searched queues":
            if (data.searched_queues.length > 0) {
                FillQueuesList(data.searched_queues);
            } 
            else {
                $('.queues-area').html("Поиск не дал результатов");
            }
            break;
//#endregion

//#region queue freeze
        case "queue freeze":
            if (data.result == "successful") {
                $('.queue-user-item').addClass("unactive-user");
                $('.queue-user-item .queue-member-function').html(userFreezeStateLabel);
                $('#member-id-' + data.member_id).addClass("unactive-user");

                indexToChangeState = userJoinedQueuesMembersCache.get(selectedQueueId).findIndex(function(element, index, array) {
                    if (element.member_id == data.member_id) {
                        return index;
                    }
                });
                members = userJoinedQueuesMembersCache.get(selectedQueueId);
                members[indexToChangeState].state = 2;
                userJoinedQueuesMembersCache.set(selectedQueueId, members);
            }
            break;
//#endregion

//#region queue member freeze (рассылка)
        case "queue member freeze":
            $('#member-id-' + data.member_id).addClass("unactive-user");

            indexToChangeState = userJoinedQueuesMembersCache.get(selectedQueueId).findIndex(function(element, index, array) {
                if (element.member_id == data.member_id) {
                    return index;
                }
            });
            members = userJoinedQueuesMembersCache.get(selectedQueueId);
            members[indexToChangeState].state = 2;
            userJoinedQueuesMembersCache.set(selectedQueueId, members);
            break;
//#endregion

//#region queue unfreeze
        case "queue unfreeze":
            if (data.result == "successful") {
                $('.queue-user-item').removeClass("unactive-user");
                $('.queue-user-item .queue-member-function').html(userActiveStateLabel);
                $('#member-id-' + data.member_id).removeClass("unactive-user");

                members = userJoinedQueuesMembersCache.get(selectedQueueId);

                //ищем, есть ли другие активные мемберы
                var anotherActiveIndex = -1;
                anotherActiveIndex = members.findIndex(function(element, index, array) {
                    return (element.state == '1' && element.member_id != data.member_id);
                });

                //индекс в кеше мембера, которого разморозили
                unfrozenMemberIndex = members.findIndex(function(element, index, array) {
                    return (element.member_id == data.member_id);
                });
                members[unfrozenMemberIndex].state = 1;

                newFirstMember = members[unfrozenMemberIndex];

                //если данный мембер единственный размороженный - кинуть на первое место,
                //остальных сдвинуть на 1 позицию
                if (anotherActiveIndex == -1) {
                                        
                    for (i = unfrozenMemberIndex - 1; i >= 0; i--) {
                        members[i+1] = members[i];
                    }

                    for (i = unfrozenMemberIndex + 1; i < members.length; i++) {
                        members[i+i].position--;
                    }
    
                    members[0] = newFirstMember;
                    members[0].position = '1';
                    
                }

                //обновление кеша
                userJoinedQueuesMembersCache.set(selectedQueueId, members);
                FillMembersList(members);
                FillUserLabel(newFirstMember);
                
            }       
            break;
//#endregion

//#region queue member unfreeze (рассылка)
        case "queue member unfreeze":
            $('#member-id-' + data.member_id).removeClass("unactive-user");

            members = userJoinedQueuesMembersCache.get(selectedQueueId);

            //ищем, есть ли другие активные мемберы
            var anotherActiveIndex = -1;
            anotherActiveIndex = members.findIndex(function(element, index, array) {
                return element.state == '1' && element.member_id != data.member_id;
            });

            //индекс в кеше мембера, которого разморозили
            unfrozenMemberIndex = members.findIndex(function(element, index, array) {
                return element.member_id == data.member_id;
            });
            members[unfrozenMemberIndex].state = 1;

            newFirstMember = members[unfrozenMemberIndex];

            //если данный мембер единственный размороженный - кинуть на первое место,
            //остальных сдвинуть на 1 позицию
            if (anotherActiveIndex == -1) {
                                        
                for (i = unfrozenMemberIndex - 1; i >= 0; i--) {
                    members[i+1] = members[i];
                }

                for (i = unfrozenMemberIndex + 1; i < members.length; i++) {
                    members[i+i].position--;
                }

                members[0] = newFirstMember;
                members[0].position = '1';
                    
            }

            //обновление кеша
            userJoinedQueuesMembersCache.set(selectedQueueId, members);

            if (selectedQueueId == data.queue_id) {
                FillMembersList(members);
                FillUserLabel(newFirstMember);
            }
            
            break;
//#endregion

//#region process member

        case "process queue member":
            
            if (data.result = "successful") {
                selectedQueueMembers = userJoinedQueuesMembersCache.get(selectedQueueId);

                selectedQueueMembers.splice(0, 1);

                if (selectedQueueMembers.length == 0) {
                    userJoinedQueuesMembersCache.set(selectedQueueId, selectedQueueMembers);
                    $('.queue-members-list').html(emptyQueueLabel);
                    $('.queue-user-item').css('display', 'none');

                    return;
                }

                var firstActiveMemberIndex = -1;
                for (i = 0; i < selectedQueueMembers.length; i++) {
                    if (selectedQueueMembers[i].state == "1") {
                        firstActiveMemberIndex = i;
                        break;
                    }
                }

                if (firstActiveMemberIndex == -1) {
                    userJoinedQueuesMembersCache.set(selectedQueueId, selectedQueueMembers);
                    DisplayMembersList(selectedQueueMembers);
                    return;
                }
                
                if (firstActiveMemberIndex != 0) {
                    
                    var nextFirst = selectedQueueMembers[firstActiveMemberIndex];

                    for (i = firstActiveMemberIndex - 1; i >= 0; i--) {
                        selectedQueueMembers[i+1] = selectedQueueMembers[i];
                    }

                    selectedQueueMembers[0] = nextFirst;
                }

                selectedQueueMembers[0].position = "1";

                for (i = firstActiveMemberIndex + 1; i < selectedQueueMembers.length; i++) {
                    if (selectedQueueMembers[i].state == '2' && selectedQueueMembers[i].position == '2') {
                        continue;
                    }
                    selectedQueueMembers[i].position--;
                }

                DisplayMembersList(selectedQueueMembers);

                userJoinedQueuesMembersCache.set(selectedQueueId, selectedQueueMembers);
            }
            break;
//#endregion

//#region queue member processed (рассылка)

        case "queue member processed":
            if (data.first_id == userId) {
                var notification = new Notification(notifyFirstMessage, 
                    {body: userLogin + notifyFirstMessageBody + '"' + data.queue_name + '"'});
            } 
            else if (data.second_id == userId) {
                var notification = new Notification(notifySecondMessage, 
                    {body: userLogin + notifySecondMessageBody + '"' + data.queue_name + '"'});
            }

            queueMembers = userJoinedQueuesMembersCache.get(data.queue_id);

            if (queueMembers == undefined) {
                return;
            } 
            else {
                ProcessDispatchHandler(data.queue_id);
            }
            break;
//#endregion

//#region kick member

        case "kick member":
            if (data.result = "successful") {
                members = userJoinedQueuesMembersCache.get(selectedQueueId);

                memberToKickIndex = members.findIndex(function(element, index, array) {
                    return element.member_id == data.member_id;
                });

                kickedMember = members.splice(memberToKickIndex, 1);
                    
                for (i = memberToKickIndex; i < members.length; i++) {
                    if (members[i].state == '2' && members[i].position == '2') {
                        continue;
                    }
                    members[i].position--;
                }

                userJoinedQueuesMembersCache.set(selectedQueueId, members);

                DisplayMembersList(members);
            }
            break;
//#endregion

//#region queue member kick (рассылка)

        case "queue member kick":
            
            if (data.second_id == userId) {
                var notification = new Notification(notifySecondMessage, 
                    {body: userLogin + notifySecondMessageBody + '"' + data.queue_name + '"'});
            }

            members = userJoinedQueuesMembersCache.get(selectedQueueId);

            if (members == undefined) {
                return;
            }

            memberToKickIndex = members.findIndex(function(element, index, array) {
                return element.member_id == data.member_id;
            });

            var kickedMember = members.splice(memberToKickIndex, 1);
            
            for (i = memberToKickIndex; i < members.length; i++) {
                if (members[i].state == '2' && members[i].position == '2') {
                    continue;
                }
                members[i].position--;
            }

            userJoinedQueuesMembersCache.set(selectedQueueId, members);

            if (selectedQueueId == data.queue_id) {
                FillMembersList(members);    

                if (kickedMember[0].user_login == userLogin) {
                    $('.queue-user-item').css('display', 'none');
                    $(".chat-area").css("display", "none");
                    $(".queue-main-info").css("width", "100%");
                    $('.queue-members-list .queue-member-function').css("display", "none");

                    $('#button-join-queue').html(joinQueueLabel);
                    $('#button-join-queue').css("display", "block");
                } 

                $('.messages-area').append(GenerateChatMessageHTML("", "Пользователь " + kickedMember[0].user_login + " удален из очереди", "Admin"));
            }

            break;
//#endregion

//#region send chat message

        case "send chat message":
            
            if (data.result == "successful") {
                var messageText = $('#message-input').val();
                $('#message-input').val("");
                var messageHTML = GenerateChatMessageHTML(userLogin, messageText, "My");
                $('.messages-area').append(messageHTML);
                if (document.body.clientWidth > 425) {
                    $('.messages-area').scrollTop($('.messages-area').prop('scrollHeight'));
                } 
                else {
                    $('.messages-area').scrollTop(0);
                }            
            }

            break;
//#endregion

//#region new chat message (рассылка)

        case "new chat message":
                    
            var messageText = data.message;
            var senderIndex = userJoinedQueuesMembersCache.get(data.queue_id).findIndex(
                function (element, index, array) {
                    return (element.member_id == data.member_id);
                }
            );
            var senderLogin = userJoinedQueuesMembersCache.get(data.queue_id)[senderIndex].user_login;
            var messageHTML = GenerateChatMessageHTML(senderLogin, messageText, "Other");
            $('.messages-area').append(messageHTML);
            if (document.body.clientWidth > 425) {
                $('.messages-area').scrollTop($('.messages-area').prop('scrollHeight'));
            } 
            else {
                $('.messages-area').scrollTop(0);
            }
            
            break;
//#endregion
        
//#region get history
        case "get history":

            if (data.result == "successful") {

                historyArray = JSON.parse(data.history);
                console.log(historyArray);

                str = GenerateHistoryListHTML(historyArray);
                $('.queue-members-list').html(str);

                isHistoryShown = true;

            }
            break;

//#endregion

//#region edit queue 
        case "edit queue":
            if (data.result == 'successful') {
                CreateQueueMode(false);
                $('.queue-info-area .queue-name').html(data.queue.queue_name);
                $('.queue-info-area .queue-place').html(data.queue.lesson_place);
                $('.queue-info-area .queue-description').html(data.queue.queue_description);
            }
            break;
//#endregion

//#region update queue (рассылка)
        case "update queue":
            //if (data.result == 'successful') {
                if (selectedQueueId == data.queue.queue_id) {
                    $('.queue-info-area .queue-name').html(data.queue.queue_name);
                    $('.queue-info-area .queue-place').html(data.queue.lesson_place);
                    $('.queue-info-area .queue-description').html(data.queue.queue_description);
                } 
            //}
            break;
//#endregion
        
//#region finish queue
        case "finish queue":
            if (data.result == "successful") {
                $('#button-finish-queue').attr("disabled", "1");        
            }
            break;

//#endregion 

//#region queue finished
        case "queue finished":
            if (selectedQueueId == data.queue_id) {
                members = userJoinedQueuesMembersCache.get(selectedQueueId);
                currentUserIndex = -1;
                currentUserIndex = members.findIndex(function(element, index, array) {
                    return (element.user_login == userLogin);
                });

                if (currentUserIndex == -1) {
                    $("#button-join-queue").css('display', 'none');
                }

            }
            break;

//#endregion
        
//#region sign out
        case "sign out":
            if (data.result == "successful") {
                userCreatedQueues = [];
                userJoinedQueues = [];
                userJoinedQueuesMembersCache = new Map();
                $('.queues-area').html("");
                ToAuthForm();  
            }
            break;
//#endregion

//#region request swap
        case "request swap":
            if (data.result == "successful") {
                $('.swap-request-form .request-message')
                .html("Заявка успешно отправлена!");
            
                $('.request-header').css('display', 'none');
                $('#button-skip-message').css('display', 'inline-block');
                $('#button-accept-request').css('display', 'none');
                $('#button-decline-request').css('display', 'none');
                $('.swap-request-form').css('display', 'flex');
            } 
            else if (data.result == "member is not online") {
                $('.swap-request-form .request-message')
                    .html("Этот пользователь сейчас не онлайн. Попробуйте еще раз позже.");
                
                $('.request-header').css('display', 'none');
                $('#button-skip-message').css('display', 'inline-block');
                $('#button-accept-request').css('display', 'none');
                $('#button-decline-request').css('display', 'none');
                $('.swap-request-form').css('display', 'flex');
            }
            else if (data.result == "order time is not passed") {
                $('.swap-request-form .request-message')
                    .html("Вы уже отправляли одно предложение менее минуты назад, и его судьба еще неизвестна. Попробуйте еще раз позже.");
                
                $('.request-header').css('display', 'none');
                $('#button-skip-message').css('display', 'inline-block');
                $('#button-accept-request').css('display', 'none');
                $('#button-decline-request').css('display', 'none');
                $('.swap-request-form').css('display', 'flex');
            }
            else if (data.result == "receiver order is not passed") {
                $('.swap-request-form .request-message')
                    .html("Этому пользователю уже сделали предложение, от которого возможно отказаться. Попробуйте еще раз позже.");
                
                $('.request-header').css('display', 'none');
                $('#button-skip-message').css('display', 'inline-block');
                $('#button-accept-request').css('display', 'none');
                $('#button-decline-request').css('display', 'none');
                $('.swap-request-form').css('display', 'flex');
            }
            break;
//#endregion

//#region new order (рассылка)
        case "new order":
            
            if (data.receiver.user_login == userLogin) {
                activeSwapOrder = {
                    queue_id: data.queue_id,
                    order_id: data.order.order_id,
                    initiator_id: data.initiator.member_id,
                    receiver_id: data.receiver.member_id
                };
                
                $('.swap-request-form .request-message')
                    .html("Участник очереди " + 
                        data.queue_name + " " + 
                        data.initiator.user_login + 
                        " (номер в очереди: " + 
                        data.initiator.position + 
                        ") предлагает Вам поменятся местами. " + 
                        "Ваше место в очереди - " + 
                        data.receiver.position + ".");
                
                $('#button-skip-message').css('display', 'none');
                $('.request-header').css('display', 'block');
                $('#button-accept-request').css('display', 'inline-block');
                $('#button-decline-request').css('display', 'inline-block');
                $('.swap-request-form').css('display', 'flex');
            }

            break;
//#endregion

//#region accept swap
        case "accept swap":
            if (data.result == "successful") {
                $('.swap-request-form').css('display', 'none');
                if (userJoinedQueuesMembersCache.has(data.queue_id)) {
                    members = userJoinedQueuesMembersCache.get(data.queue_id);

                    initiatorIndex = FindMemberIndexById(members, data.initiator.member_id);
                    receiverIndex = FindMemberIndexById(members, data.receiver.member_id);
                    
                    //swap участников очереди
                    var tmp = members[initiatorIndex];
                    members[initiatorIndex] = members[receiverIndex];
                    members[receiverIndex] = tmp;

                    //swap позиций участников
                    tmp = members[initiatorIndex].position;
                    members[initiatorIndex].position = members[receiverIndex].position;
                    members[receiverIndex].position = tmp;

                    userJoinedQueuesMembersCache.set(data.queue_id, members);

                    if (selectedQueueId == data.queue_id) {
                        FillMembersList(members);
                        FillUserLabel(FindCurrentUserByLogin(members));
                    }
                }
            } 
            else if (data.result == "order_id non-exist") {
                $('.swap-request-form .request-message')
                .html("Данная заявка более не актуальна.");
            
                $('.request-header').css('display', 'none');    
                $('#button-skip-message').css('display', 'none');
                $('#button-accept-request').css('display', 'inline-block');
                $('#button-decline-request').css('display', 'inline-block');
                $('.swap-request-form').css('display', 'flex');
            } 
            else if (data.result == "order time is passed") {
                $('.swap-request-form .request-message')
                .html("Время на принятие предложения истекло!");
            
                $('.request-header').css('display', 'none');    
                $('#button-skip-message').css('display', 'none');
                $('#button-accept-request').css('display', 'inline-block');
                $('#button-decline-request').css('display', 'inline-block');
                $('.swap-request-form').css('display', 'flex');
            }

            break;
//#endregion

//#region new swap members (рассылка)
        case "new swap members":
            if (userJoinedQueuesMembersCache.has(data.queue_id)) {
                members = userJoinedQueuesMembersCache.get(data.queue_id);

                initiatorIndex = FindMemberIndexById(members, data.initiator.member_id);
                receiverIndex = FindMemberIndexById(members, data.receiver.member_id);
                
                //swap участников очереди
                var tmp = members[initiatorIndex];
                members[initiatorIndex] = members[receiverIndex];
                members[receiverIndex] = tmp;

                //swap позиций участников
                tmp = members[initiatorIndex].position;
                members[initiatorIndex].position = members[receiverIndex].position;
                members[receiverIndex].position = tmp;

                userJoinedQueuesMembersCache.set(data.queue_id, members);

                if (selectedQueueId == data.queue_id) {
                    FillMembersList(members);
                }
            }
            break;
//#endregion

        default:
            break;
    }
}

function DisplayMembersList(members) {
    str = "";
    $('.queue-members-list').html("");
    $('.queue-members-list .queue-member-function').off('click', OnMemberFunctionClick);
    if (members.length > 0) {
        str = GenerateQueueMembersListHTML(members, isCreatorSelectedQueue);
    } 
    else {
        str = emptyQueueLabel;
    }
    $('.queue-members-list').html(str);
    $('.queue-members-list .queue-member-function').on('click', OnMemberFunctionClick);
}

function FillUserLabel(member, func="") {
    $('.queue-user-item .queue-member-number').html(member.position);
    $('.queue-user-item .queue-member-name').html(member.user_login);
    if (member.position != '1') {
        $('.queue-user-item .queue-member-function').html((member.state == 1 ? userActiveStateLabel : userFreezeStateLabel));
    } 
}

function ProcessDispatchHandler(queueId) {

    queueMembers = userJoinedQueuesMembersCache.get(queueId);

    var isCurrentUserWasDeleted = false;
    if (queueMembers[0].user_login == userLogin) {
        isCurrentUserWasDeleted = true;
    }

    queueMembers.splice(0, 1);

    if (queueMembers.length == 0) {
        userJoinedQueuesMembersCache.set(queueId, queueMembers);
        $('.queue-members-list').html(emptyQueueLabel);
        $('.queue-user-item').css('display', 'none');

        return;
    }

    var firstActiveMemberIndex = -1;
    for (i = 0; i < queueMembers.length; i++) {
        if (queueMembers[i].state == "1") {
            firstActiveMemberIndex = i;
            break;
        }
    }

    if (firstActiveMemberIndex == -1) {
        userJoinedQueuesMembersCache.set(queueId, queueMembers);
        if (selectedQueueId == queueId) {
            DisplayAfterProcessing(queueMembers, isCurrentUserWasDeleted);
        }
        return;
    }

    if (firstActiveMemberIndex != 0) {
        
        var nextFirst = queueMembers[firstActiveMemberIndex];

        for (i = firstActiveMemberIndex - 1; i >= 0; i--) {
            queueMembers[i+1] = queueMembers[i];
        }

        queueMembers[0] = nextFirst;
    }

    queueMembers[0].position = "1";

    for (i = firstActiveMemberIndex + 1; i < queueMembers.length; i++) {
        if (queueMembers[i].state == '2' && queueMembers[i].position == '2') {
            continue;
        }
        queueMembers[i].position--;
    }

    userJoinedQueuesMembersCache.set(queueId, queueMembers);
    if (selectedQueueId == queueId) {
        DisplayAfterProcessing(queueMembers, isCurrentUserWasDeleted);
    }
}

function DisplayAfterProcessing(queueMembers, isCurrentUserWasDeleted) {
    FillMembersList(queueMembers);   
    if (queueMembers[0].user_login == userLogin) {
        $('#button-join-queue').css('display', 'none');
    }

    if (isCurrentUserWasDeleted) {
        $('.queue-user-item').css('display', 'none');
        $('.queue-user-item .queue-member-function').css('display', 'block');
        $(".chat-area").css("display", "none");
        $(".queue-main-info").css("width", "100%");
        $('#button-join-queue').html(joinQueueLabel);
        $('#button-join-queue').css("display", "block");
        $('.queue-members-list .queue-member-function').css("display", "none");
    } 
    else {
        $('.queue-user-item').css('display', 'flex');
        userIndex = queueMembers.findIndex(function(element, index, array) {
            return element.user_login == userLogin;
        }); 
        FillUserLabel(queueMembers[userIndex]);
    }
}

function FindCurrentUserByLogin(members) {
    return members.find(function(element, index, array) {
        return element.user_login == userLogin;
    });
}

function FindMemberIndexById(members, id) {
    return members.findIndex(function(element, index, array) {
        return element.member_id == id;
    });
}