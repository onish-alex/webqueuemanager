const labelForAuth = "E-mail/Логин:";
const labelForReg = "E-mail:";
const regButtonLabel = "Зарегистрироваться";
const authButtonLabel = "Войти";
const switchModeAuthLabel = "Создать аккаунт";
const switchModeRegLabel = "Авторизоваться";

const authInvalidLogin = "Данного логина/почтового адреса не существует!";
const authInvalidPassword = "Неверно введенный пароль!";
const authUserExist = "Данный пользователь уже авторизован!";

const regInvalidLogin = "Логин должен быть длиной от 2 до 30 символов!";
const regInvalidPassword = "Пароль должен быть длиной от 3 символов!";
const regInvalidEmail = "Неверный формат e-mail!";
const regExistLogin = "Данный логин уже занят!";
const regExistEmail = "Данный e-mail уже занят!";
const regSuccess = "Новый пользователь успешно зарегистрирован!";

const queueInvalidDateClient = "Поле даты не заполнено!";

const queueInvalidName = "Имя очереди должно быть длиной от 1 до 100 символов";
const queueInvalidPlace = "Место должно быть длиной от 1 до 100 символов";
const queueInvalidDate = "Нельзя указывать прошедшую дату!";
const queueInvalidDescription = "Описание очереди должно быть длиной от 1 до 280 символов"

const joinQueueLabel = "Присоединится к очереди";
const leaveQueueLabel = "Покинуть очередь";
const emptyQueueLabel = "Здесь пока нет участников!";

const userActiveStateLabel = "Отойти на время";
const userFreezeStateLabel = "Вернутся в очередь";
const userSwapFunctionLabel = "Предложить поменяться";

const creatorProcessFunctionLabel = "Закончить прием";
const creatorKickFunctionLabel = "Выгнать";

const notificationRequestMessage = "Пожалуйста, разрешите сайту показывать уведомления, чтобы не пропустить свою очередь :)";
const notifyFirstMessage = "Ваша очередь наступила!";
const notifyFirstMessageBody = ", вы 1й на очереди в ";
const notifySecondMessage = "Скоро ваша очередь!";
const notifySecondMessageBody = ", вы 2й на очереди в ";