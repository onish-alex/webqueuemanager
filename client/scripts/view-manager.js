function SwitchRegAuth(authForm) {
    if (authForm.state == "Auth") {
        $(".reg-area").css("display", "block");
        $("#login-email-label").html(labelForReg);
        $("#sign-in-button").html(regButtonLabel);
        $("#switch-mode-button").html(switchModeRegLabel);
        authForm.state = "Reg";
    } 
    else {
        $(".reg-area").css("display", "none");
        $("#login-email-label").html(labelForAuth);
        $("#sign-in-button").html(authButtonLabel);
        $("#switch-mode-button").html(switchModeAuthLabel);
        authForm.state = "Auth";
    }
}

function ToMainForm(userLogin) {
    $(".auth-area").css("display", "none");
    $(".main-area").css("display", "block");
    $("#menu-panel").css("display", "flex");
    $("#salutation-label p").html("Приветствую, " + userLogin + "!");
    $('.queue-create-area').css("display", "none");
    $(".queue-form").css("display", "none");
}

function ToAuthForm() {
    $(".auth-area").css("display", "flex");
    $(".main-area").css("display", "none");
    $("#menu-panel").css("display", "none");
    $(".my-queues-buttons").css("display", "none");
    $(".error-message").html("");
    $(".queue-form").css("display", "none");
    CreateQueueMode(false);
}

function CreateQueueMode(isOn, infoToChange) {
    if (isOn) {
        $('.queue-create-area').css("display", "block");
        $('body,html').animate({scrollTop:0},500);
        $("#queue-date-input").removeAttr('disabled');
        $('#button-submit-queue').html("Создать");
        if (infoToChange != undefined && infoToChange != null) {

            $("#queue-date-input").attr('disabled', '1');

            $('#queue-name-input').val(infoToChange.name);
            $('#queue-place-input').val(infoToChange.place);
            $('#queue-description-input').val(infoToChange.description);
            
            $('#button-submit-queue').html("Применить");
        }
        
    }
    else {
        $('.queue-create-area').css("display", "none");
    }
}

function ShowConnectionError() {
    $(".auth-area").css("display", "none");
    $(".main-area").css("display", "none");
    $('.queue-create-area').css("display", "none");
    $("#menu-panel").css("display", "none");
    $(".queue-form").css("display", "none");
    $(".swap-request-form").css("display", "none");

    $('.connection-error-message').css("display", "block");
}

function ShowCreatorSelectOption(isShow) {
    if (isShow) {
        $(".hidden-option").css('display', 'block');
    } else {
        $(".hidden-option").css('display', 'none');
    }
}

function ShowUserQueuesButtons(isShow) {
    if (isShow) {
        $('.my-queues-buttons').css('display', 'inline-block');
    } else {
        $('.my-queues-buttons').css('display', 'none');
    }
}

function ShowSearchArea(isShow) {
    if (isShow) {
        $('.search').css('display', 'inline-block');
    } else {
        $('.search').css('display', 'none');
    }
}

function ToQueueForm(queue) {
    $(".main-area").css("display", "none");
    $(".queue-form").css("display", "flex");

    if (isCreatorSelectedQueue) {
        $('.history-button-area').css('display', 'inline-block');
        $('.update-button-area').css('display', 'inline-block');
        $('.finish-button-area').css('display', 'inline-block');
    } 
    else {
        $('.history-button-area').css('display', 'none');
        $('.update-button-area').css('display', 'none');
        $('.finish-button-area').css('display', 'none');
    }
    console.log(queue.queue_state);
    if (queue.queue_state == '1') {
        $('#button-finish-queue').removeAttr("disabled");       
    } 
    else {
        $('#button-finish-queue').attr("disabled", "1");       
    }
}

function FillMembersList(members) {
    
    $('.queue-user-item').css('display', 'none');
    isJoinSelectedQueue = false;
    var currentMember;
    for (i = 0; i < members.length; i++) {                
        if (members[i].user_login == userLogin) {   
            currentMember = members[i];
            isJoinSelectedQueue = true;
            break;
        } 

    }
    
    DisplayMembersList(members);

    if (!isCreatorSelectedQueue) {
        $('#button-join-queue').css('display', 'block');
    } 
    else {
        $('#button-join-queue').css('display', 'none');
        $('.queue-members-list .queue-member-function').css('display', 'block');
    }

    if (isJoinSelectedQueue) {
        $('#button-join-queue').html(leaveQueueLabel);

        $(".queue-user-item").css("display", "flex");
        $('.queue-user-item .queue-member-number').html(currentMember.position);
        $('.queue-user-item .queue-member-name').html(currentMember.user_login);
        $('.queue-members-list .queue-member-function').css('display', 'block');

        if (currentMember.state == 2) {
            $('.queue-user-item').addClass("unactive-user");
            $('.queue-user-item .queue-member-function').html(userFreezeStateLabel);
        }
        else {
            $('.queue-user-item').removeClass("unactive-user");
            $('.queue-user-item .queue-member-function').html(userActiveStateLabel);
        }
        if (currentMember.position == "1") {
            $("#button-join-queue").css('display', 'none');
            $('.queue-user-item .queue-member-function').css('display', 'none');
            $('.queue-members-list .queue-member-function').css('display', 'none');
        }
        else {
            $("#button-join-queue").css('display', 'block');
            $('.queue-user-item .queue-member-function').css('display', 'flex');
        }
    }

    if (!isJoinSelectedQueue && !isCreatorSelectedQueue) {
        $('.queue-members-list .queue-member-function').css('display', 'none');
        $('#button-join-queue').html(joinQueueLabel);
    }

    

    
///////убрать возможность поменятся с первым
    firstMember = members.find(function(element, index, array) {
        return (element.position == '1');
    });

    if (firstMember != undefined) {
        firstMemberButton = $('#member-id-' + firstMember.member_id).find(".queue-member-function");
        if (firstMember.user_login != userLogin && !isCreatorSelectedQueue) {
            firstMemberButton.css('display', 'none');
        }
        else {
            firstMemberButton.css('display', 'block');
        }
    }
///////////////
}

function FillQueuesList(queues) {

    $('.queue-item').off('click', QueueItemClickHandler);

    if (queues.length > 0) {
        str = "";
        for (i = 0; i < queues.length; i++) {
            str += GenerateQueueItemHTML(queues[i]);
            if (i == queues.length - 1) {
                lastQueueItem = queues[i];
            }
        }
        $(".queues-area").append(str);
    }
    if (queues.length < 30) {
        $('#button-load-queues').css('display', 'none');
    }

    $('.queue-item').on('click', QueueItemClickHandler);
}