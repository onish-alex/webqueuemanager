var authForm = {
    state: "Auth"
};
var mainForm = {
    state: "Global"
};
var queueCreateForm = {
    state: "Create"
}

var loadingState = "Global";
var searchStr;


var userCreatedQueues = [];
var userJoinedQueues = [];
var userJoinedQueuesMembersCache = new Map();
var userId;
var userLogin;
var lastQueueItem;
var selectedQueueId;
var selectedQueueAuthor;
var selectedQueueName;
var selectedQueueMembers;
var isJoinSelectedQueue = false;
var isFreezeSelectedQueue = false;
var isCreatorSelectedQueue = false;
var isHistoryShown = false;
var str = "";
var activeSwapOrder;
var socket = new WebSocket("ws://localhost:8090/web-queue-manager/server/server.php");

$(document).ready(function(){
    
//#region socket
    socket.onopen = function() 
    {
        //alert("Соединение установлено");
    }

    socket.onerror = function(error) 
    {
        ShowConnectionError();
    }

    socket.onclose = function() 
    {
        ShowConnectionError();
    }

    socket.onmessage = function(event) 
    {
        var data = JSON.parse(event.data);
        //alert(data.type + "     " + data.result);
        HandleResponse(data);
    }
//#endregion
    
    Notification.requestPermission();
    if (Notification.permission == "denied") {
        alert(notificationRequestMessage);
    }

    $("#login-email-label").html(labelForAuth);
    
    $("#switch-mode-button").on("click", function() {
        SwitchRegAuth(authForm);
    });

    $('#sign-in-button').on('click', function() 
    {
        let messageData;
        if (authForm.state == "Auth") {
            messageData =
            {
                type: "auth",
                login: $('#login-email').val(),
                password: $('#password').val()
            };
        }
        else {
            messageData =
            {
                type: "reg",
                login: $('#reg-login').val(),
                password: $('#password').val(),
                email: $('#login-email').val()
            };
        }
        socket.send(JSON.stringify(messageData));     
    });

    $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
            $('#button-to-top').fadeIn();
        } else {
            $('#button-to-top').fadeOut();
        }
    });
         
    $('#button-to-top').click(function() {
        $('body,html').animate({scrollTop:0},500);     
    });

    $('#button-exit').on('click', function() 
    {
        ClearSelectedQueue();
        
        let messageData =
        {
            type:"sign out"
        };
        socket.send(JSON.stringify(messageData));   
        
        
    });

    $('#button-new-queue').on('click', function() {
        queueCreateForm.state = "Create";
        CreateQueueMode(true, null);
        ClearSelectedQueue();
    });

    $('#button-all-queues').on('click', function() {

        ClearSelectedQueue();
        ToMainForm(userLogin);

        $('#order-type').val('queue_name');
        $('.queues-area').html("");
        
        let messageData = {
            type: "get global queues",
            sort_field: 'queue_name',
            last_value: null,
            last_id: null
        };

        socket.send(JSON.stringify(messageData));

        ShowCreatorSelectOption(true);
        ShowUserQueuesButtons(false);
        ShowSearchArea(true);
        mainForm.state = "Global";
    });

    $('#button-my-queues').on('click', function() {
        
        ClearSelectedQueue();
        ToMainForm(userLogin);

        let messageData =
        {
            type: "get user queues",
        };
        socket.send(JSON.stringify(messageData));

        ShowCreatorSelectOption(false);
        ShowUserQueuesButtons(true);
        ShowSearchArea(false);
        mainForm.state = "I creator";
    });

    $('#button-i-creator').on('click', function() {
        $('#order-type').val('queue_name');

        SortUserQueues('queue_name');
        str = GenerateQueueListHTML(userCreatedQueues);
        $('.queue-item').off('click', QueueItemClickHandler);
        $(".queues-area").html(str);
        $('.queue-item').on('click', QueueItemClickHandler);


        mainForm.state = "I creator";
    });

    $('#button-i-join').on('click', function() {
        $('#order-type').val('queue_name');

        SortUserQueues('queue_name');
        str = GenerateQueueListHTML(userJoinedQueues);
        $('.queue-item').off('click', QueueItemClickHandler);
        $(".queues-area").html(str);
        $('.queue-item').on('click', QueueItemClickHandler);

        mainForm.state = "I join";
    });

    $('#close-queue-create-area').on('click', function() {
        CreateQueueMode(false);
    });

    $('#button-submit-queue').on('click', function() {
        let name = $("#queue-name-input").val();
        let place = $("#queue-place-input").val();
        let description = $("#queue-description-input").val();
        
        let messageData = {
            lesson_place: place,
            queue_name: name,
            queue_description: description
        };
        

        if (queueCreateForm.state == "Create") {

            let date = $("#queue-date-input").val();

            if (date == "") {
                $(".queue-create-area > .error-message").html("<p>" + queueInvalidDateClient  + "</p>");
                return;
            }

            messageData.type = "queue create";
            messageData.lesson_date = date;

        } 
        else if (queueCreateForm.state == "Update") {
            messageData.type = "edit queue";
            messageData.queue_id = selectedQueueId;
        }

        
        socket.send(JSON.stringify(messageData));     
    });

    $('#button-load-queues').on('click', function() {
        
        if (loadingState == "Search") {
            let messageData = {
                type: "get searched queues",
                search_text: searchStr,
                last_id: lastQueueItem.queue_id,
            }
    
            socket.send(JSON.stringify(messageData));
        }
        else if (loadingState == "Global") {
            let sortField = $('#order-type').val();
    
            let messageData = {
                type: "get global queues",
                sort_field: sortField,
                last_value: lastQueueItem[sortField],
                last_id: lastQueueItem.queue_id
            };
    
            socket.send(JSON.stringify(messageData));
        }
    });

    $('#order-type').on('change', function() {
        OnOrderQueues();
    });

    $('#order-type').on('click', function() {
        if (loadingState == "Search") {
            OnOrderQueues();
        }
    });

    $('#button-join-queue').on('click', function() {
        
        let messageData = {
            queue_id: selectedQueueId
        };
        
        if ($(this).text() == joinQueueLabel) {
            messageData.type = "queue join";
        } 
        else if ($(this).text() == leaveQueueLabel) {
            messageData.type = "queue leave";
        }

        socket.send(JSON.stringify(messageData));
    });

    $('#search-submit').on('click', function() {
        
        $('.queues-area').html("");

        searchStr = $('#search-area').val();
        
        let messageData = {
            type: "get searched queues",
            search_text: searchStr,
            last_id: 0
        };

        socket.send(JSON.stringify(messageData));

        loadingState = "Search";
    });

    $('#button-send-message').on('click', function() {
        var messageText = $('#message-input').val();
        if (messageText != "") {
            let messageData = {
                type: "send chat message",
                queue_id: selectedQueueId,
                message: messageText
            }
            socket.send(JSON.stringify(messageData));
            
        }
    });

    $('#button-show-history').on('click', function() {
        if (!isHistoryShown) {
            let messageData = {
                type: "get history",
                queue_id: selectedQueueId
            };
            socket.send(JSON.stringify(messageData));
        }
        else {
            let members = userJoinedQueuesMembersCache.get(selectedQueueId);
            FillMembersList(members);
            isHistoryShown = false;
        }
    });

    $('#button-update-queue').on('click', function() {
        var infoToChange = {
            name: $('.queue-info-area .queue-name').text(),
            place: $('.queue-info-area .queue-place').text(),
            description: $('.queue-info-area .queue-description').text()
        }
        CreateQueueMode(true, infoToChange);
        queueCreateForm.state = "Update";
    });

    $('#button-finish-queue').on('click', function() {
        let messageData = {
            type: "finish queue",
            queue_id: selectedQueueId
        };
        socket.send(JSON.stringify(messageData));
    });

    $('#button-accept-request').on('click', function() {
        var messageData = {
            type: "accept swap",
            queue_id: activeSwapOrder.queue_id,
            order_id: activeSwapOrder.order_id,
            initiator_id: activeSwapOrder.initiator_id,
            receiver_id: activeSwapOrder.receiver_id
        };
        socket.send(JSON.stringify(messageData));
    });

    $('#button-decline-request').on('click', function() {
        $('.swap-request-form').css('display', 'none');
    });

    $('#button-skip-message').on('click', function() {
        $('.swap-request-form').css('display', 'none');
    });
});

function QueueItemClickHandler() {

    selectedQueueId = $(this).find('.queue-id').text();
    selectedQueueAuthor = $(this).find('.queue-author').text();
    selectedQueueName = $(this).find('.queue-name').text();

    let messageData =
    {
        type: "get queue info",
        queue_id: selectedQueueId
    };
    socket.send(JSON.stringify(messageData));

    if (userJoinedQueuesMembersCache.has(selectedQueueId)) {
        let members = userJoinedQueuesMembersCache.get(selectedQueueId);
        FillMembersList(members);
    }
    else {
        let messageData =
        {
            type: "get members",
            queue_id: selectedQueueId
        };
        socket.send(JSON.stringify(messageData));
    }
}

function ClearSelectedQueue() {
    
    let messageData = {
        type: "delete observer"
    };

    socket.send(JSON.stringify(messageData));

    if (!isJoinSelectedQueue && selectedQueueId != null) {
        userJoinedQueuesMembersCache.delete(selectedQueueId);
    }

    selectedQueueId = null;
    selectedQueueAuthor = null;
    selectedQueueName = null;
    isCreatorSelectedQueue = false;
    $('.messages-area').html("");

}

function OnOrderQueues() {
    loadingState = "Global";
    searchStr = "";
    
    let sortField = $('#order-type').val();

    switch (mainForm.state) {
        case "Global":
            $('.queues-area').html("");
    
            let messageData = {
                type: "get global queues",
                sort_field: sortField,
                last_value: null,
                last_id: null
            };

            socket.send(JSON.stringify(messageData));
            $('#button-load-queues').css('display', 'block');
            break;
        
        case "I creator":
            SortUserQueues(sortField);
            str = GenerateQueueListHTML(userCreatedQueues);
            $(".queues-area").html(str);
            break;
        
        case "I join":                    
            SortUserQueues(sortField);
            str = GenerateQueueListHTML(userJoinedQueues);
            $(".queues-area").html(str);
            break;

        default:
            break;
    }
}

function OnFreezeClick() {
    let functionLabel = $('.queue-user-item .queue-member-function').text();
    let messageData = {
        queue_id: selectedQueueId
    }
    if (functionLabel == userActiveStateLabel) {
        messageData.type = "queue freeze";
    } 
    else if (functionLabel == userFreezeStateLabel) {
        messageData.type = "queue unfreeze";  
    } 
    socket.send(JSON.stringify(messageData));
}

function OnMemberFunctionClick() {
    let messageData = {
        queue_id: selectedQueueId,
    };
    
    if (isCreatorSelectedQueue) {
        //обработать или выгнать если препод
        position = $(this).parent(".queue-member-item").find(".queue-member-number").text();
        
        if (position == '1') {
            messageData.type = "process queue member";
            messageData.member_data = $(this).parent(".queue-member-item").find(".queue-member-data").val();
            messageData.member_id = $(this).parent(".queue-member-item").find(".queue-member-id").text();

        }
        else {
            messageData.type = "kick member";
            messageData.member_id = $(this).parent(".queue-member-item").find(".queue-member-id").text();
        }
    }
    else {
        //поменятся если тоже участник
        messageData.type = 'request swap';
        messageData.initiator_id = FindCurrentUserByLogin(userJoinedQueuesMembersCache.get(selectedQueueId)).member_id;
        messageData.receiver_id = $(this).parent(".queue-member-item").find(".queue-member-id").text();
    }
    socket.send(JSON.stringify(messageData));
}
