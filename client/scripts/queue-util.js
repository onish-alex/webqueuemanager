function GenerateQueueItemHTML(item) {
    var str = '<div class="queue-item">' +
            '<p class="queue-id" style="display: none;">' + item.queue_id + '</p>' +
            '<p class="queue-name">' + item.queue_name + '</p>' +
            '<p class="queue-event-date">' + item.lesson_date + '</p>' +
            '<p class="queue-author">' + item.user_login + '</p>' +
            '<p class="queue-place">' + item.lesson_place + '</p>' +
            '<div class="queue-description">' +
            '<p>' + item.queue_description + '</p>' +
            '</div>' +
            '</div>';
    return str;
}

function GenerateQueueListHTML(queueList) {
    str="";
    for (let item of queueList) {
        str += GenerateQueueItemHTML(item);
    }
    return str;
}

function QueueNameCompare(first, second) {
    if (first.queue_name > second.queue_name) {
        return 1;
    }
    if (first.queue_name < second.queue_name) {
        return -1;
    }
    return 0;
}

function QueueDateCompare(first, second) {
    if (first.lesson_date > second.lesson_date) {
        return 1;
    }
    if (first.lesson_date < second.lesson_date) {
        return -1;
    }
    return 0;
}

function QueuePlaceCompare(first, second) {
    if (first.lesson_place > second.lesson_place) {
        return 1;
    }
    if (first.lesson_place < second.lesson_place) {
        return -1;
    }
    return 0;
}

function SortUserQueues(sortField) {
    if (sortField == 'queue_name') {
        userCreatedQueues.sort(QueueNameCompare)
    } 
    else if (sortField == 'lesson_date') {
        userCreatedQueues.sort(QueueDateCompare);
    }
    else if (sortField == 'lesson_place') {
        userCreatedQueues.sort(QueuePlaceCompare);
    }
}

function GenerateQueueMemberHTML(item, memberFunction) {
    var str = '<div id=' + '\"member-id-' + item.member_id + '\"' + ' class="queue-member-item ' + ((item.state == 2) ? 'unactive-user' : '') + '">' +
    '<div class="queue-member-id" style="display: none">' + item.member_id + '</div>' +
    '<div class="queue-member-number">' + item.position + '</div>' +
    '<div class="queue-member-name">' + item.user_login + '</div>' + 
    '<input type="text" class="queue-member-data" style="display: ' + ((memberFunction == creatorProcessFunctionLabel) ? 'block' : 'none') +  ';">';
    if (item.user_login != userLogin) {
    str += '<button class="queue-member-function">' + memberFunction + '</button>';
    };
    str += '</div>';
    return str;
}

function GenerateQueueMembersListHTML(members, isCreator) {
    str = "";
    for (i = 0; i < members.length; i++) {
        if (isCreator) {
            if (members[i].position == '1') {
               str += GenerateQueueMemberHTML(members[i], creatorProcessFunctionLabel);
               continue;
            }
            str += GenerateQueueMemberHTML(members[i], creatorKickFunctionLabel);
            continue;
        }
        str += GenerateQueueMemberHTML(members[i], userSwapFunctionLabel);
    }
    return str;
}

function ParseQueueMember(item) {
    let member = {
        member_id: item.childNodes[0].innerHTML,
        position: item.childNodes[1].innerHTML,
        user_login: item.childNodes[2].innerHTML
    };
    return member;
}   

function GetMemberList() {

    listHTML = $('.queue-members-list').html();
    list = $.parseHTML(listHTML);
    
    members = [];
    
    for (let item of list) {
        member = ParseQueueMember(item);
        console.log(member);
        members.push(member);
    }

   // console.log(list);
    console.log(members);
    return members;
}

function GenerateChatMessageHTML(userName, messageText, type) {
    var str = '<div class="chat-message ';

        switch (type) {
            case "My":
                str += 'my-chat-message';
                break;
            case "Other":
                str += 'other-chat-message';
                break;
            case "Admin":
                str += 'admin-chat-message';
                break;
        }
    
    str += '">' + 
    '<p class="sender-name" ' + ((type == "Admin") ? 'style="display: none"' : '') + '>' + userName + '</p>' +
    '<p class="message-text">' + messageText + '</p>' +
    '</div>';
    return str;
}

function GenerateHistoryItemHTML(position, item) {
    var str = '<div class="queue-member-item">' +
    '<div class="queue-member-number">' + position + '</div>' +
    '<div class="queue-member-name">' + item.user_login + '</div>' +
    '<div class="queue-member-data">' + '<p>' + item.member_data + '</p>' + '</div>' +
    '</div>';
    return str;
}

function GenerateHistoryListHTML(historyList) {
    var str = "";

    for(i = 0; i < historyList.length; i++) {
        str += GenerateHistoryItemHTML(i+1, historyList[i]);
    }

    return str;
}