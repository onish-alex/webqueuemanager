<!DOCTYPE html>
<html>
    <head>
        <title>Web Queue Manager</title>
        <link media="screen" rel="stylesheet" href="client/styles/main.css">
        <link media="screen and (max-device-width:425px)" rel="stylesheet" href="client/styles/handheld_portrait.css">
        <meta charset="utf-8">
        <script type="text/javascript" src="client/scripts/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="client/scripts/static-text.js"></script>
        <script type="text/javascript" src="client/scripts/response-handler.js"></script>
        <script type="text/javascript" src="client/scripts/queue-util.js"></script>
        <script type="text/javascript" src="client/scripts/client-manager.js"></script>
        <script type="text/javascript" src="client/scripts/view-manager.js"></script>
        <style>
            /*initial settings*/
            .reg-area, 
            .main-area, 
            #menu-panel,
            .queue-create-area, 
            .my-queues-buttons,
            .connection-error-message,
            .queue-form,
            .history-area,
            .history-button-area,
            .update-button-area,
            .finish-button-area,
            .swap-request-form {
                display: none;
            }
        </style>
    </head>
    <body>
        <?php
        include("client/pages/connection-error-message.php");
        include("client/pages/queue-create-form.php");
        include("client/pages/auth-form.php");
        include("client/pages/navigation-box.php");
        include("client/pages/queue-form.php");
        include("client/pages/main-form.php");
        include("client/pages/swap-request-form.php");
        ?>
    </body>
</html>