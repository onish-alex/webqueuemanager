<?php

class ServerManager
{

    function __construct($DBName)
    {
        $this->clientSocketByUserId = array();
        $this->validator = new ValidationManager();
        $this->dbManager = new DatabaseManager($DBName);
        $this->functionByType = 
        [
         'reg' => 'register',
         'auth' => 'authorize',
         'sign out' => 'signOut',
         'queue create' => 'createQueue',
         'queue join' => 'joinQueue',
         'queue leave' => 'leaveQueue',
         'get user queues' => 'getUserQueues',
         'get members' => 'getQueueMembers',
         'get global queues' => 'getGlobalQueues',
         'queue freeze' => 'freezeOrUnfreezeQueueMember',
         'queue unfreeze' => 'freezeOrUnfreezeQueueMember',
         'send chat message' => 'sendChatMessage',
         'process queue member' => 'processQueueMember',
         'request swap' => 'requestSwapPositions',
         'accept swap' => 'acceptSwapPositions',
         'get searched queues' => 'getGlobalQueuesBySearch',
         'edit queue' => 'editQueueByCreator',
         'delete observer' => 'deleteObserver',
         'kick member' => 'kickMemberByCreator',
         'get history' => 'getHistoryByQueueId',
         'get queue info' => 'getQueueInfo',
         'finish queue' => 'finishQueueReception'
        ];
    }

    public function executeOperation($socket, $socketManager, $data) 
    {
        $this->Log($data, "Request");
        $newData = ['type' => $data->type];

        if(array_key_exists($data->type, $this->functionByType))
        {
            $func = $this->functionByType[$data->type];
            $this->$func($socket, $data, $newData);
        }
        else 
            $newData['result'] = 'invalid type';

        if(!array_key_exists('result', $newData))
            $newData['result'] = 'successful';

        $this->Log($newData, "Response");

        if(array_key_exists('message_for_receivers', $newData))
        {
            $socketManager->sendToAll($newData['message_for_receivers'], $newData['receivers']);
            unset($newData['receivers']);
            unset($newData['message_for_receivers']);
        }
        $socketManager->sendToOne($newData, $socket);
    }

    public function Log($data, $logType) 
    {
        $log =  "Type: " . $logType . "\r\n" . "Time: " . date("Y-m-d H:i:s") . "\r\n";
        
        foreach($data as $propertyKey => $propertyValue) 
        {
            if(is_array($propertyValue)) 
            {
                $log .= $propertyKey . "\r\n";
                foreach($propertyValue as $key => $val) 
                {
                    $log .= json_encode($key) . " : " . json_encode($val) . "\r\n";
                }
            }
            else 
            {
                $log .= $propertyKey . " : " . json_encode($propertyValue) . "\r\n";
            }
        }
        $log .= "\r\n\r\n";
        file_put_contents("log.txt", $log, FILE_APPEND);
    } 

    public function deleteAllObservers() 
    {
        $this->dbManager->deleteAllObservers();
    }

    public function removeUserAuth($socket) 
    {
        $newSocketIndex = array_search($socket, $this->clientSocketByUserId);
        unset($this->clientSocketByUserId[$newSocketIndex]);
    } 

    public function fillReceivers($receiversArr, $ownerId, $currentUserId, &$newData) 
    {
        $newData['receivers'] = array();
        foreach($receiversArr as $receiver) 
        {
            if(array_key_exists($receiver->user_fid, $this->clientSocketByUserId) && 
            ($currentUserId == null || ($currentUserId != null && $receiver->user_fid != $currentUserId))) 
            {
                $newData['receivers'][] = $this->clientSocketByUserId[$receiver->user_fid];
            }
        }

        if($ownerId != null && array_key_exists($ownerId, $this->clientSocketByUserId)) 
        {
            $newData['receivers'][] = $this->clientSocketByUserId[$ownerId];
        }
    }




    public function signOut($socket, $data, &$newData) 
    {
        $this->removeUserAuth($socket);
    }

    public function authorize($socket, $data, &$newData) 
    {
        $resultArr = $this->dbManager->selectUserByLogin($data->login);

        if(!$this->validator->assertLogin($resultArr, $newData) ||
           !$this->validator->assertAuthExist($resultArr[0]->user_id, $socket, $this->clientSocketByUserId, $newData) ||
           !$this->validator->assertPassword($data->password, $resultArr[0]->user_password, $newData))
           return;

        $this->clientSocketByUserId[$resultArr[0]->user_id] = $socket;
        $newData['user_login'] = $resultArr[0]->user_login;
        $newData['user_id'] = $resultArr[0]->user_id;
    }

    public function register($socket, $data, &$newData) 
    {
        if(!$this->validator->assertStrData($data->login, 2, 30, 'invalid login', $newData) ||
           !$this->validator->assertEmail($data->email, $newData) ||
           !$this->validator->assertStrData($data->password, 3, 256, 'invalid password', $newData))
            return;

        $resultArr = $this->dbManager->selectUserByLoginOrEmail($data->login, $data->email);

        if(!$this->validator->assertLoginOrEmailExist($resultArr, $data->login, $newData))
            return;

        $hashedPassword = password_hash($data->password, PASSWORD_DEFAULT);
        $this->dbManager->insertUser($data->login, $data->email, $hashedPassword);
    }

    public function createQueue($socket, $data, &$newData) 
    {
        $now = date("Y-m-d H:i:s");

        if(!$this->validator->assertStrData($data->queue_name, 1, 100, 'invalid queue_name', $newData) ||
           !$this->validator->assertStrData($data->lesson_place, 1, 100, 'invalid lesson_place', $newData) ||
           !$this->validator->assertLessonDate($data->lesson_date, $now, $newData) ||
           !$this->validator->assertStrData($data->queue_description, 1, 280, 'invalid queue_description', $newData))
            return;

        $userId = array_search($socket, $this->clientSocketByUserId);

        $newData['queue'] = $this->dbManager->insertQueue($now, $data->lesson_date, $data->lesson_place,
         $data->queue_name, $data->queue_description, $userId)[0];
    }

    public function getUserQueues($socket, $data, &$newData) 
    {
        $userId = array_search($socket, $this->clientSocketByUserId);
        $newData['user_queues'] = $this->dbManager->selectUserQueues($userId);
    }

    public function getGlobalQueues($socket, $data, &$newData) 
    {
        $newData['global_queues'] = $this->dbManager->selectGlobalQueues($data->sort_field,
         30, $data->last_value, $data->last_id);
    }

    public function deleteObserver($socket, $data, &$newData) 
    {
        $userId = array_search($socket, $this->clientSocketByUserId);
        $this->dbManager->deleteObserver($userId);
    }

    public function getGlobalQueuesBySearch($socket, $data, &$newData) 
    {
        $newData['searched_queues'] = $this->dbManager->selectGlobalQueuesBySearch($data->search_text, $data->last_id);
    }

    public function getQueueInfo($socket, $data, &$newData) 
    {
        $queues = $this->dbManager->selectQueueById($data->queue_id);
        $newData['queue'] = count($queues) != 0 ? $queues[0] : null;
    }

    public function getHistoryByQueueId($socket, $data, &$newData) 
    {
        $queues = $this->dbManager->selectQueueById($data->queue_id);   
        $newData['history'] = count($queues) != 0 ? $queues[0]->queue_history : null;
    }

    public function getQueueMembers($socket, $data, &$newData) 
    {
        $userId = array_search($socket, $this->clientSocketByUserId);
        $newData['queue_members'] = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);

        $queues = $this->dbManager->selectQueueById($data->queue_id);   
        $newData['queue'] = count($queues) != 0 ? $queues[0] : null;

        $resultMember = array_filter($newData['queue_members'], function ($m) use ($userId) { return $m->user_fid == $userId; });

        if(count($resultMember) == 0 && 
           count($queues) != 0 &&
           $newData['queue']->queue_creator != $userId)
            $this->dbManager->insertMember($userId, $data->queue_id, -1, 3);
    }

    public function sendChatMessage($socket, $data, &$newData) 
    {
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);

        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultMember = array_filter($resultMemberArr, function ($m) use ($userId) { return $m->user_fid == $userId; });
        $currentMember = end($resultMember);

        if(!$this->validator->assertMemberExist($resultMember, false, $newData) ||
           !$this->validator->assertStrData($data->message, 1, 1000, 'invalid message', $newData))
            return;

        $this->fillReceivers($resultMemberArr, null, $currentMember->user_fid, $newData);
        $newData['message_for_receivers']['type'] = 'new chat message';
        $newData['message_for_receivers']['queue_id'] = $data->queue_id;
        $newData['message_for_receivers']['member_id'] = $currentMember->member_id;
        $newData['message_for_receivers']['message'] = $data->message;
    }

    public function editQueueByCreator($socket, $data, &$newData) 
    {
        if(!$this->validator->assertStrData($data->queue_name, 1, 100, 'invalid queue_name', $newData) ||
           !$this->validator->assertStrData($data->lesson_place, 1, 100, 'invalid lesson_place', $newData) ||
           !$this->validator->assertStrData($data->queue_description, 1, 280, 'invalid queue_description', $newData))
            return;
        
        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);
    
        if(!$this->validator->assertQueueExist($resultQueueArr, $newData) ||
           !$this->validator->assertQueueCreator($userId, $resultQueueArr[0]->queue_creator, $newData) )
            return;

        $this->dbManager->updateQueue($data->queue_name, $data->lesson_place, $data->queue_description, $data->queue_id);

        $newData['queue'] = $resultQueueArr[0];
        $newData['queue']->queue_name = $data->queue_name;
        $newData['queue']->lesson_place = $data->lesson_place;
        $newData['queue']->queue_description = $data->queue_description;
    
        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);
        $this->fillReceivers($resultMemberArr, null, null, $newData);
    
        $newData['message_for_receivers']['type'] = 'update queue';
        $newData['message_for_receivers']['queue'] = $newData['queue'];
    }

    public function finishQueueReception($socket, $data, &$newData) 
    {
        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData) ||
           !$this->validator->assertQueueCreator($userId, $resultQueueArr[0]->queue_creator, $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);

        $this->dbManager->updateQueueState(2, $data->queue_id);
        

        $newData['queue_id'] = $data->queue_id;
        
        $this->fillReceivers($resultMemberArr, null, null, $newData);
        
        $newData['message_for_receivers']['type'] = 'queue finished';
        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
    }

    public function kickMemberByCreator($socket, $data, &$newData) 
    {
        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData) ||
           !$this->validator->assertQueueCreator($userId, $resultQueueArr[0]->queue_creator, $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);
        $memberId = $data->member_id;
        $resultMember = array_filter($resultMemberArr, function ($m) use ($memberId) { return $m->member_id == $memberId; });
            
        if(!$this->validator->assertMemberExist($resultMember, false, $newData) ||
           !$this->validator->assertPositionEqualToOne(end($resultMember)->position, true, $newData))
            return;
    
        $currentMember = end($resultMember);
        $position = $currentMember->position;
        $membersForUpdate = array_filter( $resultMemberArr, function ($m) use ($position) { return $m->position > $position; } );
    
        $this->dbManager->updateMemberPositions($membersForUpdate);

        if(array_key_exists(end($resultMember)->user_fid, $this->clientSocketByUserId)) 
        {
            $this->dbManager->updateMember(3, -1, $currentMember->member_id);
        }
        else 
        {
            $this->dbManager->deleteMember($currentMember->member_id);
        }
    
        $newData['queue_id'] = $currentMember->queue_fid;
        $newData['member_id'] = $currentMember->member_id;
    
        $this->fillReceivers($resultMemberArr, null, null, $newData);
    
        $newData['message_for_receivers']['type'] = 'queue member kick';
        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
        $newData['message_for_receivers']['member_id'] = $newData['member_id'];
        $newMembers = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);
        $newData['message_for_receivers']['second_id'] = count($newMembers) > 1 && $currentMember->position == 2 ? $newMembers[1]->user_fid : null;
        $newData['message_for_receivers']['queue_name'] = $resultQueueArr[0]->queue_name;
    }




    public function joinQueue($socket, $data, &$newData) 
    {
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData) ||
           !$this->validator->assertQueueActive(end($resultQueueArr), $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);

        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultMember = array_filter($resultMemberArr, function ($m) use ($userId) { return $m->user_fid == $userId; });
        
        if(!$this->validator->assertMemberExist($resultMember, true, $newData))
            return;

        $unfreezeMembers = array_filter($resultMemberArr, function ($m) { return $m->state == 1; });

        $currentMember = end($resultMember);
        $position = count($unfreezeMembers) == 0 ? 1 : end($resultMemberArr)->position + 1;

        $this->dbManager->updateMember(1, $position, $currentMember->member_id);
        $newData['queue_member']['member_id'] = $currentMember->member_id;
        $newData['queue_member']['user_login'] = $currentMember->user_login;
        $newData['queue_member']['queue_id'] = $currentMember->queue_fid;
        $newData['queue_member']['user_id'] = $currentMember->user_fid;
        $newData['queue_member']['position'] = $position;
        $newData['queue_member']['state'] = 1;

        $this->fillReceivers($resultMemberArr, $resultQueueArr[0]->queue_creator, $userId, $newData);

        $newData['message_for_receivers']['type'] = 'new queue member';
        $newData['message_for_receivers']['queue_member'] = $newData['queue_member'];
    }

    public function leaveQueue($socket, $data, &$newData) 
    {
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);

        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultMember = array_filter($resultMemberArr, function ($m) use ($userId) { return $m->user_fid == $userId; });
        
        if(!$this->validator->assertMemberExist($resultMember, false, $newData) ||
           !$this->validator->assertPositionEqualToOne(end($resultMember)->position, true, $newData))
            return;

        $currentMember = end($resultMember);
        $position = $currentMember->position;
        $membersForUpdate = array_filter( $resultMemberArr, function ($m) use ($position) { return $m->position > $position; } );

        $this->dbManager->updateMemberPositions($membersForUpdate);
        $this->dbManager->updateMember(3, -1, $currentMember->member_id);

        $newData['queue_id'] = $currentMember->queue_fid;
        $newData['member_id'] = $currentMember->member_id;
        $newData['queue_state'] = $resultQueueArr[0]->queue_state;

        $this->fillReceivers($resultMemberArr, $resultQueueArr[0]->queue_creator, $currentMember->user_fid, $newData);

        $newData['message_for_receivers']['type'] = 'queue member leave';
        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
        $newData['message_for_receivers']['member_id'] = $newData['member_id'];
        $newMembers = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);
        $newData['message_for_receivers']['second_id'] = count($newMembers) > 1 && $currentMember->position == 2 ? $newMembers[1]->user_fid : null;
        $newData['message_for_receivers']['queue_name'] = $resultQueueArr[0]->queue_name;
    }

    public function freezeOrUnfreezeQueueMember($socket, $data, &$newData) 
    {
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);

        $userId = array_search($socket, $this->clientSocketByUserId);
        $resultMember = array_filter($resultMemberArr, function ($m) use ($userId) { return $m->user_fid == $userId; });

        $currentMember = end($resultMember);
        
        if(!$this->validator->assertMemberExist($resultMember, false, $newData) ||
           !$this->validator->assertPositionEqualToOne($currentMember->position, true, $newData))
            return;

        if($data->type == 'queue freeze') {
            $this->dbManager->updateMember(2, $currentMember->position, $currentMember->member_id);
            $newData['message_for_receivers']['type'] = 'queue member freeze';
        }
        else if($data->type == 'queue unfreeze') {
            $unfreezeMembers = array_filter($resultMemberArr, function ($m) { return $m->state == 1; });
            if(count($unfreezeMembers) == 0) 
            {
                $position = $currentMember->position;
                $membersForUpdate = array_filter( $resultMemberArr, function ($m) use ($position) { return $m->position > $position; } );
        
                $this->dbManager->updateMemberPositions($membersForUpdate);
                $this->dbManager->updateMember(1, 1, $currentMember->member_id);
            }
            else 
            {
                $this->dbManager->updateMember(1, $currentMember->position, $currentMember->member_id);
            }
            $newData['message_for_receivers']['type'] = 'queue member unfreeze';
        }
        $newData['queue_id'] = $currentMember->queue_fid;
        $newData['member_id'] = $currentMember->member_id;

        $this->fillReceivers($resultMemberArr, $resultQueueArr[0]->queue_creator, $currentMember->user_fid, $newData);

        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
        $newData['message_for_receivers']['member_id'] = $newData['member_id'];
    }

    public function processQueueMember($socket, $data, &$newData) 
    {
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        if(!$this->validator->assertQueueExist($resultQueueArr, $newData))
            return;

        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);

        $userId = array_search($socket, $this->clientSocketByUserId);
        
        $memberId = $data->member_id;
        $resultMember = array_filter($resultMemberArr, function($m) use ($memberId) { return $m->member_id == $memberId; });
        $memberForDelete = end($resultMember);

        if(!$this->validator->assertMemberExist($resultMember, false, $newData) ||
           !$this->validator->assertPositionEqualToOne($memberForDelete->position, false, $newData) ||
           !$this->validator->assertQueueCreator($userId, $resultQueueArr[0]->queue_creator, $newData) ||
           !$this->validator->assertStrData($data->member_data, 0, 100, 'invalid member_data', $newData))
            return;

        if(array_key_exists($memberForDelete->user_fid, $this->clientSocketByUserId)) 
            $this->dbManager->updateMember(3, -1, $memberForDelete->member_id);
        else 
            $this->dbManager->deleteMember($memberForDelete->member_id);

        $history = json_decode($resultQueueArr[0]->queue_history);
        $historyObject = ['user_login' => $memberForDelete->user_login];
        $historyObject['member_data'] = $data->member_data;
        $history[] = $historyObject;
    
        $this->dbManager->updateHistory(json_encode($history), $resultQueueArr[0]->queue_id);
        
        $unfreezeMembers = array_filter($resultMemberArr, function ($m) { return $m->state == 1; });
        
        current($unfreezeMembers);
        next($unfreezeMembers);
        $member = current($unfreezeMembers);
        echo "\n\n";
        if($member != null) 
        {
            $position = $member->position;
            $membersForUpdate = array_filter( $resultMemberArr, function ($m) use ($position) { return $m->position >= $position; } );
            $index = array_search($member, $membersForUpdate);
            $membersForUpdate[$index]->position = 2;
            $this->dbManager->updateMemberPositions($membersForUpdate);
        }

        $newData['queue_id'] = $memberForDelete->queue_fid;
        $newData['member_id'] = $memberForDelete->member_id;

        $this->fillReceivers($resultMemberArr, null, null, $newData);

        $newData['message_for_receivers']['type'] = 'queue member processed';
        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
        $newData['message_for_receivers']['member_id'] = $newData['member_id'];
        $newMembers = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);
        $newData['message_for_receivers']['first_id'] = count($newMembers) != 0 ? $newMembers[0]->user_fid : null;
        $newData['message_for_receivers']['second_id'] = count($newMembers) > 1 ? $newMembers[1]->user_fid : null;
        $newData['message_for_receivers']['queue_name'] = $resultQueueArr[0]->queue_name;
    }

    public function requestSwapPositions($socket, $data, &$newData) 
    {
        $resultMemberArr = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);

        $userId = array_search($socket, $this->clientSocketByUserId);
        $initiatorId = $data->initiator_id;
        $resultInitiator = array_filter($resultMemberArr, function ($m) use ($userId) { return $m->user_fid == $userId; });

        $receiverId = $data->receiver_id;
        $resultReceiver = array_filter($resultMemberArr, function ($m) use ($receiverId) { return $m->member_id == $receiverId; });

        if(!$this->validator->assertMemberExistAndEqual($resultInitiator, $initiatorId, $newData) ||
           !$this->validator->assertMemberExist($resultReceiver, false, $newData) ||
           !$this->validator->assertPositionEqualToOne(end($resultInitiator)->position, true, $newData) ||
           !$this->validator->assertPositionEqualToOne(end($resultReceiver)->position, true, $newData) ||
           !$this->validator->assertMemberOnline(end($resultReceiver)->user_fid, $this->clientSocketByUserId, $newData))
            return;

        $orders = $this->dbManager->selectOrderByInitiatorId($userId);
        $ordersRec = $this->dbManager->selectOrderByReceiverId(end($resultReceiver)->user_fid);
        
        $now = date("Y-m-d H:i:s");

        if(!$this->validator->assertReceiverOrder($ordersRec, $now, $newData))
            return; 
        
        if(count($orders) != 0 ) 
        {
            if(!$this->validator->assertOrderDatePassed($orders[0]->order_date, $now, false, $newData))
                return;

            $this->dbManager->deleteOrder($orders[0]->order_id);
        }

        $order = $this->dbManager->insertOrder(end($resultInitiator)->user_fid, end($resultReceiver)->user_fid, $now)[0];

        $newData['queue_id'] = $data->queue_id;
        $newData['order'] = $order;
        $newData['initiator'] = end($resultInitiator);
        $newData['receiver'] = end($resultReceiver);
        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);
        $newData['queue_name'] = $resultQueueArr[0]->queue_name;

        $newData['message_for_receivers']['type'] = 'new order';
        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
        $newData['message_for_receivers']['queue_name'] = $newData['queue_name'];
        $newData['message_for_receivers']['order'] = $newData['order'];
        $newData['message_for_receivers']['initiator'] = $newData['initiator'];
        $newData['message_for_receivers']['receiver'] = $newData['receiver'];
        $newData['receivers'] = array();
        $newData['receivers'][] = $this->clientSocketByUserId[end($resultReceiver)->user_fid];
    }

    public function acceptSwapPositions($socket, $data, &$newData) 
    {
        $resultMemberArr = $this->dbManager->selectMembersByQueueIdWithoutObservers($data->queue_id);

        $initiatorId = $data->initiator_id;
        $resultInitiator = array_filter($resultMemberArr, function ($m) use ($initiatorId) { return $m->member_id == $initiatorId; });

        $userId = array_search($socket, $this->clientSocketByUserId);
        $receiverId = $data->receiver_id;
        $resultReceiver = array_filter($resultMemberArr, function ($m) use ($userId) { return $m->user_fid == $userId; });

        $userId = array_search($socket, $this->clientSocketByUserId);

        if(!$this->validator->assertMemberExistAndEqual($resultReceiver, $receiverId, $newData) ||
           !$this->validator->assertMemberExist($resultInitiator, false, $newData) ||
           !$this->validator->assertPositionEqualToOne(end($resultInitiator)->position, true, $newData) ||
           !$this->validator->assertPositionEqualToOne(end($resultReceiver)->position, true, $newData))
            return;

        $orders = $this->dbManager->selectOrderById($data->order_id);

        if(!$this->validator->assertOrderExist($orders, $newData) ||
           !$this->validator->assertOrderData(end($orders)->initiator_id, end($resultInitiator)->user_fid, $newData) ||
           !$this->validator->assertOrderData(end($orders)->receiver_id, end($resultReceiver)->user_fid, $newData) ||
           !$this->validator->assertOrderDatePassed(end($orders)->order_date, date("Y-m-d H:i:s"), true, $newData))
            return;

        $initiator = end($resultInitiator);
        $receiver = end($resultReceiver);

        $this->dbManager->updateMember($initiator->state, $receiver->position, $initiator->member_id);
        $this->dbManager->updateMember($receiver->state, $initiator->position, $receiver->member_id);
        $this->dbManager->deleteNonValidOrders(end($resultInitiator)->user_fid, end($resultReceiver)->user_fid);

        $resultQueueArr = $this->dbManager->selectQueueById($data->queue_id);

        $newData['queue_id'] = $data->queue_id;
        $newData['order'] = end($orders);
        $newData['queue_name'] = $resultQueueArr[0]->queue_name;
        $position = $initiator->position;
        $newData['initiator'] = $initiator;
        $newData['initiator']->position = $receiver->position;
        $newData['receiver'] = $receiver;
        $newData['receiver']->position = $position;


        $resultMemberArr = $this->dbManager->selectMembersByQueueId($data->queue_id);
        
        $this->fillReceivers($resultMemberArr, $resultQueueArr[0]->queue_creator, $userId, $newData);

        $newData['message_for_receivers']['type'] = 'new swap members';
        $newData['message_for_receivers']['queue_id'] = $newData['queue_id'];
        $newData['message_for_receivers']['queue_name'] = $newData['queue_name'];
        $newData['message_for_receivers']['order'] = $newData['order'];
        $newData['message_for_receivers']['initiator'] = $newData['initiator'];
        $newData['message_for_receivers']['receiver'] = $newData['receiver'];
    }


}