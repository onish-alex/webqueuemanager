<?php

define("PORT", "8090");
define("HOST", "localhost");
define("DBNAME", "queue_db");

require_once ("socket-manager.php");
require_once ("server-manager.php");
require_once ("validation-manager.php");
require_once ("database-manager.php");

$socketManager = new SocketManager(HOST, PORT);
$serverManager = new ServerManager(DBNAME);

$serverManager->DeleteAllObservers();
file_put_contents("log.txt", "");

$socket = $socketManager->createSocket();

$clientSocketArray = array($socket);


while(true) 
{
    $newSocketArray = $clientSocketArray;
    $nullA = [];
    socket_select($newSocketArray, $nullA, $nullA, 0, 0);

    if(in_array($socket, $newSocketArray)) 
    {
        $newSocket = socket_accept($socket);
        $clientSocketArray[] = $newSocket;
        $header = socket_read($newSocket, 1024);
        $socketManager->sendHeaders($header, $newSocket);

        $newSocketIndex = array_search($socket, $newSocketArray);
        unset($newSocketArray[$newSocketIndex]);
    }

    foreach($newSocketArray as $newSocketResource) 
    {

        while(@socket_recv($newSocketResource, $socketData, 1024, 0) >= 1) 
        {
            $data = $socketManager->unseal($socketData);
            if(isset($data->type)) {
                $serverManager->executeOperation($newSocketResource, $socketManager, $data);
                break 2;
            }
        }

        $socketData = @socket_read($newSocketResource, 1024, PHP_NORMAL_READ);
        if($socketData === false) 
        {
            $serverManager->DeleteObserver($newSocketResource, null, $newSocketResource);
            $serverManager->removeUserAuth($newSocketResource);
            $newSocketIndex = array_search($newSocketResource, $clientSocketArray);
            unset($clientSocketArray[$newSocketIndex]);
        }

    }

}

socket_close($socket);

?>