
<?php

class SocketManager 
{
    private $port;
    private $host;

    function __construct($host, $port)
    {
        $this->port = $port;
        $this->host = $host;
    }


    public function createSocket() 
    {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_set_option($socket, SOL_SOCKET, SO_KEEPALIVE, 1);
        socket_bind($socket, 0, (int)($this->port));
        socket_listen($socket);
        return $socket;
    }

    public function sendHeaders($headersText, $newSocket) 
    {
        $headers = array();
        $tmpLine = preg_split("/\r\n/", $headersText);

        foreach($tmpLine as $line) 
        {
            $line = rtrim($line);
            if(preg_match('/\A(\S+): (.*)\z/', $line, $matches)) 
            {
                $headers[$matches[1]] = $matches[2];
            }
        }

        print_r($headers);
        $key = $headers['Sec-WebSocket-Key'];
        $sKey = base64_encode(pack('H*', sha1($key.'258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));

        $strHeaders = 
            "HTTP/1.1 101 Switching Protocols \r\n" .
            "Upgrade: websocket\r\n" .
            "Connection: Upgrade\r\n" .
            "WebSocket-Origin: $this->host\r\n" .
            "WebSocket-Location: ws://$this->host:$this->port/web-queue-manager/server/server.php\r\n" .
            "Sec-WebSocket-Accept:$sKey\r\n\r\n";

        socket_write($newSocket, $strHeaders, strlen($strHeaders));
    }


    private function seal($object) 
    {
        $socketData = json_encode($object);
        $b1 = 0x81;
        $length = strlen($socketData);
        $header = "";

        if($length <= 125) 
        {
            $header = pack('CC', $b1, $length);
        } 
        else if($length > 125 && $length < 65536) 
        {
            $header = pack('CCn', $b1, 126, $length);
        }
        else 
        {
            $header = pack('CCNN', $b1, 127, $length);
        }
        return $header . $socketData;
    }

    public function unseal($object) 
    {
        $length = ord($object[1]) & 127;

        if($length == 126) 
        {
            $mask = substr($object, 4, 4);
            $data = substr($object,  8);
        }
        else if ($length == 127) 
        {
            $mask = substr($object, 10, 4);
            $data = substr($object,  14);
        }
        else 
        {
            $mask = substr($object, 2, 4);
            $data = substr($object,  6);
        }

        $socketStr = "";

        for($i = 0; $i < strlen($data); ++$i) 
        {
            $socketStr .= $data[$i] ^ $mask[$i%4];
        }

        return json_decode($socketStr);
    }

    public function sendToAll($object, $socketArray) 
    {
        $message = $this->seal($object);
        $messageLength = strlen($message);

        foreach($socketArray as $socket) 
        {
            @socket_write($socket, $message, $messageLength);
        }

        return true;
    }

    public function sendToOne($object, $socket) 
    {
        $this->sendToAll($object, array($socket));
    }


}