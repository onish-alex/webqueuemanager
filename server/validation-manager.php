<?php

class ValidationManager
{

    public function assertLogin($users , &$newData) 
    {
        if(count($users) == 0)
        {
            $newData['result'] = 'invalid login';
            return false;
        }
        return true;
    }

    public function assertAuthExist($userId, $socket, $clientSocketByUserId, &$newData) 
    {
        if(array_key_exists($userId, $clientSocketByUserId)) 
        {
            $newData['result'] = 'user auth exist';
            return false;
        }
        if(array_search($socket, $clientSocketByUserId) != false) 
        {
            $newData['result'] = 'socket exist';
            return false;
        }
        return true;
    }

    public function assertPassword($password, $userPassword, &$newData) 
    {
        if(!password_verify($password, $userPassword)) 
        {
            $newData['result'] = 'invalid password';
            return false;
        }
        return true;
    }

    public function assertStrData($data, $min, $max, $errorMessage, &$newData) 
    {
        if(strlen($data) < $min || strlen($data) > $max) 
        {
            $newData['result'] = $errorMessage;
            return false;
        }
        return true;
    }

    public function assertEmail($email, &$newData) 
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) 
        {
            $newData['result'] = 'invalid email';
            return false;
        }
        return true;
    }

    public function assertLoginOrEmailExist($users, $login, &$newData) 
    {
        if(count($users) != 0)
        {
            if($users[0]->user_login == $login) 
                $newData['result'] = 'exist login';
            else 
                $newData['result'] = 'exist email';
            
            return false;
        }
        return true;
    }

    public function assertLessonDate($date, $now, &$newData) 
    {
        if($date == "" || $now > $date) 
        {
            $newData['result'] = 'invalid lesson_date';
            return false;
        }
        return true;
    }

    public function assertQueueExist($queues, &$newData) 
    {
        if(count($queues) == 0)
        {
            $newData['result'] = 'queue_id non-exist';
            return false;
        }
        return true;
    }

    public function assertQueueActive($queue, &$newData) 
    {
        if($queue->queue_state != 1)
        {
            $newData['result'] = 'queue is not active';
            return false;
        }
        return true;
    }

    public function assertMemberExist($members, $isValidateExist, &$newData) 
    {
        if(count($members) != 0 && $isValidateExist && end($members)->state != 3) 
        {
            $newData['result'] = 'queue_member exist';
            return false;
        }
        if(!$isValidateExist && ((count($members) == 0) || (count($members) != 0 && end($members)->state == 3))) 
        {
            $newData['result'] = 'queue_member non-exist';
            return false;
        }
        return true;
    }

    public function assertPositionEqualToOne($position, $isValidateEqual, &$newData) 
    {
        if(($position == 1 && $isValidateEqual) ||
           ($position != 1 && !$isValidateEqual) ) 
        {
            $newData['result'] = 'invalid member position';
            return false;
        }
        return true;
    }

    public function assertQueueCreator($userId, $creatorId, &$newData) 
    {
        if($userId != $creatorId) 
        {
            $newData['result'] = 'user is not creator';
            return false;
        }
        return true;
    }

    public function assertMemberExistAndEqual($memberArr, $memberId, &$newData) 
    {
        if(count($memberArr) == 0 || end($memberArr)->member_id != $memberId) 
        {
            $newData['result'] = 'invalid sender';
            return false;
        }
        return true;
    }

    public function assertOrderExist($orders, &$newData) 
    {
        if(count($orders) == 0)
        {
            $newData['result'] = 'order_id non-exist';
            return false;
        }
        return true;
    }

    public function assertOrderDatePassed($orderDate, $now, $isPassed, &$newData) 
    {
        
        $secondDifference = ((new DateTime($now))->getTimestamp() - (new DateTime($orderDate))->getTimestamp()); 
        if($secondDifference < 60 && !$isPassed)
        {
            $newData['result'] = 'order time is not passed';
            return false;
        }
        if($secondDifference > 60 && $isPassed)
        {
            $newData['result'] = 'order time is passed';
            return false;
        }
        return true;
    }

    public function assertOrderData($dataId, $userId, &$newData) 
    {
        if($dataId != $userId)
        {
            $newData['result'] = 'invalid order';
            return false;
        }
        return true;
    }

    public function assertMemberOnline($userId, $users, &$newData) 
    {
        if(!array_key_exists($userId, $users)) 
        {
            $newData['result'] = 'member is not online';
            return false;
        }
        return true;
    }


    public function assertReceiverOrder($orders, $now, &$newData) 
    {
        if(count($orders) == 0)
            return true;

        $secondDifference = ((new DateTime($now))->getTimestamp() - (new DateTime(end($orders)->order_date))->getTimestamp()); 
        
        if($secondDifference < 60) {
            $newData['result'] = 'receiver order is not passed';
            return false;
        }
        
        return true;
    }

}