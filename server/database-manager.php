<?php

class DatabaseManager
{
        
    function __construct($DBName)
    {
        $this->dsn = "mysql:host=localhost;port=3306;dbname=$DBName;charset=utf8";
        $this->pdo = new PDO($this->dsn, 'root', '');


        $this->stmtSelectUserByLogin = $this->pdo->prepare(
            "select * from users where user_login = ? or user_email = ?");

        $this->stmtSelectUserByLoginOrEmail = $this->pdo->prepare(
            "select * from users where user_login = ? or user_email = ? or user_login = ? or user_email = ?");

        $this->stmtInsertUser = $this->pdo->prepare(
            "insert into users (user_id, user_login, user_email, user_password) values (NULL, ?, ?, ?)");

        $this->stmtDeleteAllObservers = $this->pdo->prepare(
            "delete from queue_members where state = 3");

        $this->stmtInsertQueue = $this->pdo->prepare(
            "insert into queues (queue_id, create_date, lesson_date, lesson_place, queue_name, queue_description, queue_creator,"
            . " queue_history, queue_state) values (NULL, ?, ?, ?, ?, ?, ?, \"\", 1)");

        $this->stmtSelectQueuesByUserId = $this->pdo->prepare(
            "select distinct queues.*, users.user_login from queues, users, queue_members where "
            . "((queues.queue_id in(select queue_fid from queue_members where user_fid = ?)) or "
            . "(queue_creator = ?)) and users.user_id = queues.queue_creator order by queue_name");

        $this->stmtDeleteObserver = $this->pdo->prepare(
            "delete from queue_members where user_fid = ? and state = 3");

        $this->stmtSelectGlobalQueuesBySearch = $this->pdo->prepare(
            "select queues.*, users.user_login from queues, users where (queues.queue_creator = users.user_id "
            . "and (queues.queue_name like ? or queues.lesson_place like ? or "
            . " queues.queue_description like ? or users.user_login like ?) and queue_id > ?) and queue_state = 1 "
            . "order by queue_id limit 30");

        $this->stmtSelectQueueById = $this->pdo->prepare(
            "select * from queues where queue_id = ?");

        $this->stmtSelectMembersByQueueIdWithoutObservers = $this->pdo->prepare(
            "select queue_members.*, users.user_login from queue_members, users where queue_members.queue_fid = ?"
            . " and queue_members.user_fid = users.user_id and queue_members.state != 3 order by position");

        $this->stmtInsertMember = $this->pdo->prepare(
            "insert into queue_members (member_id, user_fid, queue_fid, position, state)"
            . " values (NULL, ?, ?, ?, ?);");

        $this->stmtUpdateMember = $this->pdo->prepare(
            "update queue_members set state = ?, position = ? where member_id = ?");
            
        
        $this->stmtSelectMembersByQueueId = $this->pdo->prepare(
            "select queue_members.*, users.user_login from queue_members, users where queue_members.queue_fid = ?"
            . " and queue_members.user_fid = users.user_id order by position");
        
        $this->stmtSelectNewQueue = $this->pdo->prepare(
            "select queues.*, users.user_login from queues, users where queue_creator = user_id order by queue_id desc limit 1");
        $this->stmtSelectNewMember = $this->pdo->prepare(
            "select queue_members.*, users.user_login from queue_members, users where user_fid = user_id order by member_id desc limit 1");
        
        $this->stmtDeleteMember = $this->pdo->prepare(
            "delete from queue_members where member_id = ?");
        
        
        $this->stmtUpdateHistory = $this->pdo->prepare(
            "update queues set queue_history = ? where queue_id = ?");
        $this->stmtUpdateQueue = $this->pdo->prepare(
                "update queues set queue_name = ?, lesson_place = ?, queue_description = ?  where queue_id = ?");
        $this->stmtUpdateQueueState = $this->pdo->prepare(
            "update queues set queue_state = ? where queue_id = ?");
        
        

        $this->stmtInsertOrder = $this->pdo->prepare(
            "insert into orders (order_id, initiator_id, receiver_id, order_date) values (NULL, ?, ?, ?)");
        $this->stmtSelectOrderById = $this->pdo->prepare(
            "select * from orders where order_id = ?");
        $this->stmtSelectOrderByInitiatorId = $this->pdo->prepare(
            "select * from orders where initiator_id = ?");
        $this->stmtSelectOrderByReceiverId = $this->pdo->prepare(
            "select * from orders where receiver_id = ?");
        $this->stmtSelectMemberOrders = $this->pdo->prepare(
            "select * from orders where initiator_id = ? or receiver_id = ?");
        $this->stmtSelectNewOrder = $this->pdo->prepare(
            "select * from orders order by order_id desc limit 1");
        $this->stmtDeleteOrder = $this->pdo->prepare(
            "delete from orders where order_id = ?");
        $this->stmtDeleteNonValidOrders = $this->pdo->prepare(
            "delete from orders where initiator_id = ? or receiver_id = ? or initiator_id = ? or receiver_id = ?");
    }

    #region insert queries

    public function insertUser($login, $email, $hashedPassword) 
    {
        $this->stmtInsertUser->execute(array($login, $email, $hashedPassword));
    }

    public function insertQueue($now, $lessonDate, $lessonPlace, $queueName, $queueDescription, $userId) 
    {
        $this->stmtInsertQueue->execute(array($now, $lessonDate, $lessonPlace, $queueName, $queueDescription, $userId));
        $this->stmtSelectNewQueue->execute();
        return $this->stmtSelectNewQueue->fetchAll(PDO::FETCH_OBJ);
    }

    public function insertMember($userId, $queueId, $position, $state) 
    {
        $this->stmtInsertMember->execute(array($userId, $queueId, $position, $state));
        $this->stmtSelectNewMember->execute();
        return $this->stmtSelectNewMember->fetchAll(PDO::FETCH_OBJ);
    }


    public function insertOrder($initiator, $receiver, $date) 
    {
        $this->stmtInsertOrder->execute(array($initiator, $receiver, $date));
        $this->stmtSelectNewOrder->execute();
        return $this->stmtSelectNewOrder->fetchAll(PDO::FETCH_OBJ);
    }


    #endregion


    #region select queries
    
    public function selectUserByLogin($login) 
    {
        $this->stmtSelectUserByLogin->execute(array($login, $login));
        return $this->stmtSelectUserByLogin->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function selectUserByLoginOrEmail($login, $email) 
    {
        $this->stmtSelectUserByLoginOrEmail->execute(array($login, $login, $email, $email));
        return $this->stmtSelectUserByLoginOrEmail->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectUserQueues($userId) 
    {
        $this->stmtSelectQueuesByUserId->execute(array($userId, $userId));
        return $this->stmtSelectQueuesByUserId->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectGlobalQueues($sort, $count, $lastValue, $lastQueueId) 
    {
        if($sort != "queue_name" && $sort != "lesson_date" &&
           $sort != "queue_creator" && $sort != "lesson_place")
            return array();
        
        if($lastValue == null && $lastQueueId == null) 
        {
            $stmt = $this->pdo->prepare(
                "select queues.*, users.user_login from queues, users "
                . "where queue_creator = user_id and queue_state = 1 order by " . $sort . ", queue_id limit ?, ?");

            $start = 0;
        }
        else 
        {
            $stmtCount = $this->pdo->prepare(
                "select count(queues.queue_id) as queue_count from queues where (" . $sort . " < ? "
                ."or (" . $sort . " = ? and queues.queue_id < ?)) and queue_state = 1");

            $stmt = $this->pdo->prepare(
                "select queues.*, users.user_login from queues, users "
                . "where queue_creator = user_id and queue_state = 1 order by " . $sort . ", queue_id limit ?, ?");

            $stmtCount->execute(array($lastValue, $lastValue, $lastQueueId));
            $start = $stmtCount->fetchAll(PDO::FETCH_OBJ)[0]->queue_count + 1;

        }
        
        $stmt->bindValue(1, $start, PDO::PARAM_INT);
        $stmt->bindValue(2, $count, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectGlobalQueuesBySearch($searchText, $lastId) 
    {
        $this->stmtSelectGlobalQueuesBySearch->execute(array("%".$searchText."%", "%".$searchText."%", 
        "%".$searchText."%", "%".$searchText."%", $lastId));
        return $this->stmtSelectGlobalQueuesBySearch->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectQueueById($queueId) 
    {
        $this->stmtSelectQueueById->execute(array($queueId));
        return $this->stmtSelectQueueById->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectMembersByQueueIdWithoutObservers($queueId) 
    {
        $this->stmtSelectMembersByQueueIdWithoutObservers->execute(array($queueId));
        return $this->stmtSelectMembersByQueueIdWithoutObservers->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectMembersByQueueId($queueId) 
    {
        $this->stmtSelectMembersByQueueId->execute(array($queueId));
        return $this->stmtSelectMembersByQueueId->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectOrderById($orderId) 
    {
        $this->stmtSelectOrderById->execute(array($orderId));
        return $this->stmtSelectOrderById->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function selectOrdersByMemberId($userId) 
    {
        $this->stmtSelectMemberOrders->execute(array($userId, $userId));
        return $this->stmtSelectMemberOrders->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectOrderByInitiatorId($initiatorId) 
    {
        $this->stmtSelectOrderByInitiatorId->execute(array($initiatorId));
        return $this->stmtSelectOrderByInitiatorId->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectOrderByReceiverId($receiverId) 
    {
        $this->stmtSelectOrderByReceiverId->execute(array($receiverId));
        return $this->stmtSelectOrderByReceiverId->fetchAll(PDO::FETCH_OBJ);
    }

    

    #endregion


    #region update queries

    public function updateMemberPositions($membersForUpdate) 
    {
        if(count($membersForUpdate) != 0) {
            $sqlQuery = "insert into queue_members (member_id, position) values ";
            $sqlQuery .= substr(str_repeat('(?, ?),', count($membersForUpdate)), 0, -1);
            $sqlQuery .= " on duplicate key update position = values(position)";

            $stmt = $this->pdo->prepare($sqlQuery);

            $i = 1;
            foreach($membersForUpdate as $m) 
            {
                $stmt->bindParam($i++, $m->member_id);
                $stmt->bindValue($i++, $m->position - 1, PDO::PARAM_INT);
            }
            $stmt->execute();
        }
    }

    public function updateMember($state, $position, $memberId) 
    {
        $this->stmtUpdateMember->execute(array($state, $position, $memberId));
    }
    
    public function updateQueue($queueName, $lessonPlace, $queueDescription, $queueId) 
    {
        $this->stmtUpdateQueue->execute(array($queueName, $lessonPlace, $queueDescription, $queueId));
    } 

    public function updateQueueState($state, $queueId) 
    {
        $this->stmtUpdateQueueState->execute(array($state, $queueId));
    } 

    public function updateHistory($history, $queueId) 
    {
        $this->stmtUpdateHistory->execute(array($history, $queueId));
    } 


    #endregion


    #region delete queries

    public function deleteAllObservers()
    {
        $this->stmtDeleteAllObservers->execute();
    }

    public function deleteObserver($userId) 
    {
        $this->stmtDeleteObserver->execute(array($userId));
    }

    public function deleteMember($memberId) 
    {
        $this->stmtDeleteMember->execute(array($memberId));
    }
    

    public function deleteOrder($orderId)
    {
        $this->stmtDeleteOrder->execute(array($orderId));
    }

    public function deleteNonValidOrders($initiatorId, $receiverId)
    {
        $this->stmtDeleteNonValidOrders->execute(array($initiatorId, $initiatorId, $receiverId, $receiverId));
    }

    
    #endregion

}